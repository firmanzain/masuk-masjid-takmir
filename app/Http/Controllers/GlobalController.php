<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GlobalController extends Controller
{

    public function refreshCsrfToken(){
        return ['csrfToken' => csrf_token()];
    }
}
