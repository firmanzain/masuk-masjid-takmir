window.Vue = require('vue');
import numeral from 'numeral'
import moment from 'moment'

const globalMethods = Vue.mixin({
    methods: {
        refreshTokens() {
          return new Promise((resolve, reject) => {
              axios.get('/refreshtoken')
                  .then( response => {
                      window.axios.defaults.headers.common['X-CSRF-TOKEN'] = response.data.csrfToken;
                      resolve(response);
                  })
                  .catch( error => {
                      this.showErrorMessage(error.message);
                      reject(error);
                  });
          });
        },
        deleteDefaultHeaders() {
          delete axios.defaults.headers.common['X-CSRF-TOKEN'];
          delete axios.defaults.headers.common['X-Requested-With'];
        },
        async checkAuth() {

          this.deleteDefaultHeaders()

          try {
              let response = await axios.get(`${this.globalVar.apiUrl}check`, {
                  headers: this.globalVar.headers
              })

              console.log("===response", response);

              this.globalVar.session = response.data;
              this.globalVar.deviceAvailable = response.data.mosque.package.count;
              this.globalVar.statusVerification = response.data.mosque.status;

              if (response.data.mosque.status < 0) {
                  if (this.$route.path != "/verification") {
                      this.$router.replace('/verification')
                  }
              } else {
                  if (this.$route.path == "/verification" || this.$route.path == "/" || this.$route.path == "/login") {
                      this.$router.replace('/mosque')
                  }
              }

              // if (this.$route.path == "/" || this.$route.path == "/login") {
              //     this.$router.replace('/mosque')
              // }

          } catch (error) {
              if (this.$route.path != "/") {
                this.$router.replace('/')
              }
          }
        },
        doLogout() {
            let self = this
            self.deleteDefaultHeaders()

            axios.post(`${self.globalVar.apiUrl}logout`, {}, {
                headers: self.globalVar.headers,
            })
            .then(response => {
                Toast.fire({
                    type: 'success',
                    title: 'Session destroy'
                })
                localStorage.removeItem('token')
                window.location = '/'
            })
            .catch(error => {
                Swal.fire(
                    'Alert!',
                    'Something went wrong.',
                    'warning'
                )
                this.refreshTokens()
            });
        },
        isNumber(event) {
          event = (event) ? event : window.event;
          var charCode = (event.which) ? event.which : event.keyCode;
          if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            event.preventDefault();
          } else {
            return true;
          }
        },
        formatNumber(value) {
            let formatted = numeral(value).format('0,0')
            return formatted
        },
        parseNumber(value) {
            value = value.toString()
            let parse = parseInt(value.replace(/\,/g, ""))
            return parse
        },
        formatDateDefault(date) {
            return moment(date).format('YYYY-MM-DD');
        },
        // formattedAutocompleteItems(result) {
        //     return ' (' + result.code + ') ' + result.name
        // },
        statusLabel(status) {
            if (status == 0) {
                return '<span class="badge badge-danger">Non Aktif</span>';
            } else {
                return '<span class="badge badge-success">Aktif</span>';
            }
        },
        dataURItoBlob(dataURI) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            //Old Code
            //write the ArrayBuffer to a blob, and you're done
            //var bb = new BlobBuilder();
            //bb.append(ab);
            //return bb.getBlob(mimeString);

            //New Code
            return new Blob([ab], {type: mimeString});
        },
        sleep(ms) {
          return new Promise(resolve => setTimeout(resolve, ms));
        },
        showProgressBar() {
          Swal.fire({
            html: 'Please Wait . . . <br><br><div class="progress"><div id="progress" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div></div>',
            onBeforeOpen: () => {
              Swal.showLoading()
            },
          })
        },
        goback() {
          this.$router.go(-1)
        },
    },
    filters: {
        formatNumber: function (value) {
            let formatted = numeral(value).format('0,0')
            return formatted
        },
        formatDateTime: function (date) {
            if (date) {
                return moment(date).format("DD/MM/YYYY HH:mm");
            } else {
                return null
            }
        },
        formatDate: function (date) {
            if (date) {
                return moment(date).format("DD/MM/YYYY");
            } else {
                return null
            }
        },
        // typeSaleLabel: function (status) {
        //     if (status == 1) {
        //         return 'Barang';
        //     } else {
        //         return 'Jasa';
        //     }
        // },
    },
})

export default globalMethods
