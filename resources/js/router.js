window.Vue = require('vue');
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const Login = () => import(
  /* webpackChunkName: "js/login" */ './components/auth/Login.vue')
const Dashboard = () => import(
  /* webpackChunkName: "js/dashboard" */ './components/backoffice/Dashboard.vue')
const Mosque = () => import(
  /* webpackChunkName: "js/mosque" */ './components/backoffice/Mosque.vue')
const Takmir = () => import(
  /* webpackChunkName: "js/takmir" */ './components/backoffice/Takmir.vue')
const Device = () => import(
  /* webpackChunkName: "js/device" */ './components/backoffice/Device.vue')
const TextRunning = () => import(
  /* webpackChunkName: "js/text-running" */ './components/backoffice/TextRunning.vue')
const TextInfo = () => import(
  /* webpackChunkName: "js/text-info" */ './components/backoffice/TextInfo.vue')
const TextArabic = () => import(
  /* webpackChunkName: "js/text-arabic" */ './components/backoffice/TextArabic.vue')
const Preacher = () => import(
  /* webpackChunkName: "js/preacher" */ './components/backoffice/Preacher.vue')
const Donature = () => import(
  /* webpackChunkName: "js/donature" */ './components/backoffice/Donature.vue')
const ImageBackground = () => import(
  /* webpackChunkName: "js/image-background" */ './components/backoffice/ImageBackground.vue')
// const ImagePhoto = () => import(
//   /* webpackChunkName: "js/image-photo" */ './components/backoffice/ImagePhoto.vue')
const SettingGeneral = () => import(
  /* webpackChunkName: "js/setting-general" */ './components/backoffice/SettingGeneral.vue')
const ContentLayoutInformation = () => import(
  /* webpackChunkName: "js/content-informative" */ './components/backoffice/ContentLayoutInformation.vue')
const ContentLayoutClock = () => import(
  /* webpackChunkName: "js/content-clock" */ './components/backoffice/ContentLayoutClock.vue')
const Event = () => import(
  /* webpackChunkName: "js/event" */ './components/backoffice/Event.vue')
const Verification = () => import(
  /* webpackChunkName: "js/verification" */ './components/backoffice/Verification.vue')

let routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    meta: {
      title: 'Login | ',
    }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      title: 'Dashboard | ',
    },
    redirect: '/mosque'
  },
  {
    path: '/mosque',
    name: 'mosque',
    component: Mosque,
    meta: {
      title: 'Informasi Masjid | ',
    }
  },
  {
    path: '/takmir',
    name: 'takmir',
    component: Takmir,
    meta: {
      title: 'Takmir | ',
    }
  },
  {
    path: '/device',
    name: 'device',
    component: Device,
    meta: {
      title: 'Media TV | ',
    }
  },
  {
    path: '/text/running/:layout?',
    name: 'text-running',
    component: TextRunning,
    meta: {
      title: 'Teks Berjalan | ',
    }
  },
  {
    path: '/text/info',
    name: 'text-info',
    component: TextInfo,
    meta: {
      title: 'Pesan Singkat | ',
    }
  },
  {
    path: '/text/arabic',
    name: 'text-arabic',
    component: TextArabic,
    meta: {
      title: 'Teks Arab dan Terjemahan | ',
    }
  },
  {
    path: '/preacher',
    name: 'preacher',
    component: Preacher,
    meta: {
      title: 'Khotib dan Bilal | ',
    }
  },
  {
    path: '/donature',
    name: 'donature',
    component: Donature,
    meta: {
      title: 'Donatur | ',
    }
  },
  {
    path: '/image/background/:layout?',
    name: 'image-background',
    component: ImageBackground,
    meta: {
      title: 'Gambar Latar | ',
    }
  },
  // {
  //   path: '/image/photo',
  //   name: 'image-photo',
  //   component: ImagePhoto,
  //   meta: {
  //     title: 'Galeri Foto | ',
  //   }
  // },
  {
    path: '/setting/general',
    name: 'setting-general',
    component: SettingGeneral,
    meta: {
      title: 'Pengaturan Umum | ',
    }
  },
  {
    path: '/content/informative',
    name: 'content-informative',
    component: ContentLayoutInformation,
    meta: {
      title: 'Layout Informatif | ',
    }
  },
  {
    path: '/content/clock',
    name: 'content-clock',
    component: ContentLayoutClock,
    meta: {
      title: 'Layout Hanya Jam | ',
    }
  },
  {
    path: '/event',
    name: 'event',
    component: Event,
    meta: {
      title: 'Acara | ',
    }
  },
  {
    path: '/verification',
    name: 'verification',
    component: Verification,
    meta: {
      title: 'Setup Awal | ',
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  routes, // short for `routes: routes`
  scrollBehavior: function(to) {
      if (to.hash) {
          return {selector: to.hash}
      } else {
          return { x: 0, y: 0 }
      }
  },
})

export default router
