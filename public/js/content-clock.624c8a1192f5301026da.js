(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/content-clock"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {},
  mounted: function mounted() {},
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar
    };
  },
  methods: {},
  filters: {},
  activated: function activated() {
    var self = this;
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=template&id=fd05db48&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=template&id=fd05db48& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Layout Informatif")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c(
        "div",
        {
          staticClass: "container-fluid",
          staticStyle: { "background-color": "#fff" }
        },
        [
          _c("div", { staticClass: "row justify-content-md-center mb-3" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("img", {
                staticClass: "img-tv",
                attrs: {
                  src: _vm.globalVar.publicPath + "images/layout-clock.jpeg",
                  alt: ""
                }
              })
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "row justify-content-md-center mb-3",
              staticStyle: { margin: "10px" }
            },
            [
              _vm._m(1),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-4" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "media-icon-link",
                      attrs: { to: "/mosque" }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "info-box bg-light pointer shadow rounded"
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "info-box-content text-center" },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "cold-lg-6 col-md-12 col-sm-12 mb-2"
                                  },
                                  [
                                    _c("img", {
                                      staticClass: "layout-icon",
                                      attrs: {
                                        src:
                                          _vm.globalVar.publicPath +
                                          "images/icon/masjid.png",
                                        alt: ""
                                      }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "cold-lg-6 col-md-12 col-sm-12"
                                  },
                                  [
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-number text-center text-muted mb-0"
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Informasi Masjid\n                                    "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-text text-center text-muted",
                                        staticStyle: { "white-space": "unset" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Segala informasi tentang nama masjid, alamat dll\n                                    "
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-4" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "media-icon-link",
                      attrs: { to: "/setting/general" }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "info-box bg-light pointer shadow rounded"
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "info-box-content text-center" },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "cold-lg-6 col-md-12 col-sm-12 mb-2"
                                  },
                                  [
                                    _c("img", {
                                      staticClass: "layout-icon",
                                      attrs: {
                                        src:
                                          _vm.globalVar.publicPath +
                                          "images/icon/datetime.png",
                                        alt: ""
                                      }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "cold-lg-6 col-md-12 col-sm-12"
                                  },
                                  [
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-number text-center text-muted mb-0"
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Jadwal Sholat\n                                    "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-text text-center text-muted",
                                        staticStyle: { "white-space": "unset" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Pengaturan lama waktu adzan, iqomah dan sholat\n                                    "
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-4" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "media-icon-link",
                      attrs: { to: "/image/background/clock" }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "info-box bg-light pointer shadow rounded"
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "info-box-content text-center" },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "cold-lg-6 col-md-12 col-sm-12 mb-2"
                                  },
                                  [
                                    _c("img", {
                                      staticClass: "layout-icon",
                                      attrs: {
                                        src:
                                          _vm.globalVar.publicPath +
                                          "images/icon/photo.png",
                                        alt: ""
                                      }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "cold-lg-6 col-md-12 col-sm-12"
                                  },
                                  [
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-number text-center text-muted mb-0"
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Gambar Latar\n                                    "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-text text-center text-muted",
                                        staticStyle: { "white-space": "unset" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Gambar latar yg akan ditampilkan\n                                    "
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-4" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "media-icon-link",
                      attrs: { to: "/text/running/clock" }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "info-box bg-light pointer shadow rounded"
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "info-box-content text-center" },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "cold-lg-6 col-md-12 col-sm-12 mb-2"
                                  },
                                  [
                                    _c("img", {
                                      staticClass: "layout-icon",
                                      attrs: {
                                        src:
                                          _vm.globalVar.publicPath +
                                          "images/icon/teks.png",
                                        alt: ""
                                      }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "cold-lg-6 col-md-12 col-sm-12"
                                  },
                                  [
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-number text-center text-muted mb-0"
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Teks Berjalan\n                                    "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-text text-center text-muted",
                                        staticStyle: { "white-space": "unset" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Informasi teks berjalan pada bagian bawah\n                                    "
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-4" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "media-icon-link",
                      attrs: { to: "/text/arabic" }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "info-box bg-light pointer shadow rounded"
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "info-box-content text-center" },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "cold-lg-6 col-md-12 col-sm-12 mb-2"
                                  },
                                  [
                                    _c("img", {
                                      staticClass: "layout-icon",
                                      attrs: {
                                        src:
                                          _vm.globalVar.publicPath +
                                          "images/icon/teks.png",
                                        alt: ""
                                      }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "cold-lg-6 col-md-12 col-sm-12"
                                  },
                                  [
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-number text-center text-muted mb-0"
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Teks Arab\n                                    "
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "span",
                                      {
                                        staticClass:
                                          "info-box-text text-center text-muted",
                                        staticStyle: { "white-space": "unset" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                        Teks dalam bahasa arab beserta terjemahannya\n                                    "
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    ]
                  )
                ],
                1
              )
            ]
          )
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Layout Informatif")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "col-md-12 text-center text-uppercase mb-3 font-weight-bold"
      },
      [
        _c("h2", { staticStyle: { color: "#0E4FBA", "font-weight": "1000" } }, [
          _vm._v("\n                    Pengaturan Konten\n                ")
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/ContentLayoutClock.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/components/backoffice/ContentLayoutClock.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ContentLayoutClock_vue_vue_type_template_id_fd05db48___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ContentLayoutClock.vue?vue&type=template&id=fd05db48& */ "./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=template&id=fd05db48&");
/* harmony import */ var _ContentLayoutClock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ContentLayoutClock.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ContentLayoutClock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ContentLayoutClock_vue_vue_type_template_id_fd05db48___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ContentLayoutClock_vue_vue_type_template_id_fd05db48___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/ContentLayoutClock.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContentLayoutClock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ContentLayoutClock.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContentLayoutClock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=template&id=fd05db48&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=template&id=fd05db48& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContentLayoutClock_vue_vue_type_template_id_fd05db48___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ContentLayoutClock.vue?vue&type=template&id=fd05db48& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/ContentLayoutClock.vue?vue&type=template&id=fd05db48&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContentLayoutClock_vue_vue_type_template_id_fd05db48___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContentLayoutClock_vue_vue_type_template_id_fd05db48___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);