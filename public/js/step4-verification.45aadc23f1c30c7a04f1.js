(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/step4-verification"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step4.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Step4.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    this.loadData();
    Fire.$on('AfterLoaded', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    var sortOrders = {};
    var columns = [{
      width: '5%',
      label: 'No.',
      sortable: false
    }, {
      label: 'Id Media TV',
      name: 'number',
      sortable: true
    }, {
      label: 'Tipe',
      name: 'type',
      sortable: true
    }, {
      label: 'OTP',
      name: 'otp',
      sortable: true
    }, {
      label: 'OTP Valid',
      name: 'otp_valid',
      sortable: true
    }, {
      label: 'Konfirmasi',
      name: 'status_confirmation',
      sortable: true
    }, {
      label: 'Versi Aplikasi',
      name: 'application_version',
      sortable: true
    }, {
      label: 'Status',
      name: 'status',
      sortable: true
    }, {
      width: '25%',
      label: 'Action',
      sortable: false
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      globalVar: this.$attrs.globalVar,
      renderingCountType: 0,
      selecteditemsType: [{
        id: 2,
        name: 'Informatif'
      }, {
        id: 1,
        name: 'Hanya Jam'
      }],
      renderingCountType2: 0,
      selecteditemsType2: [{
        id: 2,
        name: 'Informatif'
      }, {
        id: 1,
        name: 'Hanya Jam'
      }],
      dataTables: [],
      columns: columns,
      sortKey: 'name',
      sortOrders: sortOrders,
      perPage: ['10', '25', '50', '100'],
      tableData: {
        draw: 0,
        length: 10,
        search: '',
        column: 1,
        dir: 'asc',
        acp: 'masjid'
      },
      pagination: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      },
      form: new Form({
        id: '',
        number: '',
        name: '',
        type: ''
      }),
      formVerification: new Form({
        id: '',
        number: '',
        type: ''
      })
    };
  },
  methods: {
    changeType: function changeType() {
      var self = this;
      var typeId = 0;

      if ($("#type").select2('data')[0]) {
        typeId = $("#type").select2('data')[0].id;
      }

      self.form.type = parseInt(typeId);
    },
    changeType2: function changeType2() {
      var self = this;
      var typeId = 0;

      if ($("#type2").select2('data')[0]) {
        typeId = $("#type2").select2('data')[0].id;
      }

      self.formVerification.type = parseInt(typeId);
    },
    configPagination: function configPagination(data) {
      this.pagination.lastPage = data.last_page;
      this.pagination.currentPage = data.current_page;
      this.pagination.total = data.total;
      this.pagination.lastPageUrl = data.last_page_url;
      this.pagination.nextPageUrl = data.next_page_url;
      this.pagination.prevPageUrl = data.prev_page_url;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
      this.tableData.column = this.getIndex(this.columns, 'name', key);
      this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
      this.loadData();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    loadData: function loadData() {
      var self = this;
      self.deleteDefaultHeaders();
      self.tableData.draw++;
      axios.get("".concat(self.globalVar.apiUrl, "device"), {
        params: self.tableData,
        headers: self.globalVar.headers
      }).then(function (response) {
        var data = response.data;

        if (self.tableData.draw == data.draw) {
          self.dataTables = data.data.data;
          self.configPagination(data.data);
        }
      })["catch"](function (errors) {});
    },
    openModalVerification: function openModalVerification() {
      var _this2 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      $("#type2").select2("destroy");
      this.formVerification.clear();
      this.formVerification.reset();
      $("#modalFormVerification").modal("show");

      if (data) {
        this.formVerification.id = data.id;
        document.getElementById("type2").selectedIndex = 0;
      }

      $("#type2").select2({
        width: '100%'
      });
      this.changeType2();
      setTimeout(function () {
        _this2.$refs.number_verification.focus();
      }, 400);
    },
    submitDataVerification: function submitDataVerification() {
      var self = this;
      self.$Progress.start();
      self.showProgressBar();
      this.deleteDefaultHeaders();
      axios.put("".concat(self.globalVar.apiUrl, "device/verification/").concat(self.formVerification.id), self.formVerification, {
        headers: self.globalVar.headers,
        onUploadProgress: function onUploadProgress(progressEvent) {
          var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100));
          $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
        }
      }).then(function (response) {
        Toast.fire({
          type: 'success',
          title: 'OTP generate successfully'
        });
        self.$Progress.finish();
        Fire.$emit('AfterLoaded');
        $("#modalFormVerification").modal("hide");
      })["catch"](function (error) {
        var message = 'Something went wrong.';

        if (error.response) {
          message = error.response.data.message;
          Swal.fire('Alert!', message, 'warning');
        }

        self.$Progress.fail();
      });
    },
    generateOtp: function generateOtp(data) {
      var _this3 = this;

      var self = this;
      Swal.fire({
        title: 'Are you sure?',
        text: 'OTP will be regenerate.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          _this3.deleteDefaultHeaders();

          axios.get("".concat(self.globalVar.apiUrl, "device/otp/regenerate/").concat(data.id), {
            headers: self.globalVar.headers
          }).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterLoaded');
          })["catch"](function (error) {
            var message = 'Something went wrong.';

            if (error.response) {
              message = error.response.data.message;
              Swal.fire('Alert!', message, 'warning');
            }
          });
        }
      });
    },
    openModal: function openModal() {
      var _this4 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      $("#type").select2("destroy");
      this.form.clear();
      this.form.reset();
      $("#modalForm").modal("show");

      if (data) {
        this.form.fill(data);

        if (this.form.type == 1) {
          document.getElementById("type").selectedIndex = 1;
        } else {
          document.getElementById("type").selectedIndex = 0;
        }
      }

      $("#type").select2({
        width: '100%'
      });
      setTimeout(function () {
        _this4.$refs.name.focus();
      }, 400);
    },
    submitData: function submitData() {
      var self = this;
      self.$Progress.start();
      self.showProgressBar();
      this.deleteDefaultHeaders();
      var method = '';

      if (self.form.id) {
        method = axios.put("".concat(self.globalVar.apiUrl, "device/").concat(self.form.id), self.form, {
          headers: self.globalVar.headers,
          onUploadProgress: function onUploadProgress(progressEvent) {
            var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100));
            $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
          }
        });
      } else {
        method = axios.post("".concat(self.globalVar.apiUrl, "device"), self.form, {
          headers: self.globalVar.headers,
          onUploadProgress: function onUploadProgress(progressEvent) {
            var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100));
            $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
          }
        });
      }

      method.then(function (response) {
        Toast.fire({
          type: 'success',
          title: 'Data updated successfully'
        });
        self.$Progress.finish();
        Fire.$emit('AfterLoaded');
        $("#modalForm").modal("hide");
      })["catch"](function (error) {
        var message = 'Something went wrong.';

        if (error.response) {
          message = error.response.data.message;
          Swal.fire('Alert!', message, 'warning');
        }

        self.$Progress.fail();
      });
    },
    activeData: function activeData(data) {
      var _this5 = this;

      var self = this;
      var status = 0;
      var text = 'Record will be Deactivated';

      if (data.status == 0) {
        status = 1;
        text = 'Record will be Activated';
      }

      Swal.fire({
        title: 'Are you sure?',
        text: text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          _this5.deleteDefaultHeaders();

          axios.put("".concat(self.globalVar.apiUrl, "device/status/").concat(data.id), {
            status: status
          }, {
            headers: self.globalVar.headers
          }).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterLoaded');
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    },
    deleteData: function deleteData(data) {
      var _this6 = this;

      var self = this;
      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          _this6.deleteDefaultHeaders();

          axios["delete"]("".concat(self.globalVar.apiUrl, "device/").concat(data.id), {
            headers: self.globalVar.headers
          }).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterLoaded');
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    },
    showUpdateAlert: function showUpdateAlert() {
      Swal.fire('Alert!', 'Perbarui versi aplikasi dari device untuk mendapatkan update terbaru.', 'warning');
    }
  },
  filters: {
    censored: function censored(id) {
      var random = Math.floor(Math.random() * (id.length - 1)) + 1;
      var idx = 0;
      var formatted = '';

      for (var i = 0; i < id.length; i++) {
        if (i < 4) {
          formatted += id[i];
        } else if (i == id.length - 4) {
          formatted += id[i];
        } else if (i == id.length - 3) {
          formatted += id[i];
        } else if (i == id.length - 2) {
          formatted += id[i];
        } else if (i == id.length - 1) {
          formatted += id[i];
        } else {
          formatted += 'X';
        }
      }

      return formatted;
    }
  },
  activated: function activated() {
    var self = this;
    Fire.$emit('AfterLoaded');
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step4.vue?vue&type=template&id=4e9ebc67&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Step4.vue?vue&type=template&id=4e9ebc67& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row p-4" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h5", [
          _c("b", [_vm._v("Device tersedia:")]),
          _vm._v(
            " " +
              _vm._s(_vm.pagination.total) +
              "/" +
              _vm._s(_vm.globalVar.deviceAvailable) +
              "\n          "
          )
        ]),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "dataTables_wrapper scroll" },
          [
            _c(
              "div",
              {
                staticClass: "dataTables_length",
                attrs: { id: "tableData_length" }
              },
              [
                _c("label", [
                  _vm._v("Show\n                      "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.tableData.length,
                          expression: "tableData.length"
                        }
                      ],
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              _vm.tableData,
                              "length",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          },
                          function($event) {
                            return _vm.loadData()
                          }
                        ]
                      }
                    },
                    _vm._l(_vm.perPage, function(records, index) {
                      return _c(
                        "option",
                        { key: index, domProps: { value: records } },
                        [_vm._v(_vm._s(records))]
                      )
                    }),
                    0
                  ),
                  _vm._v("\n                      entries\n                  ")
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "dataTables_filter",
                attrs: { id: "tableData_filter" }
              },
              [
                _c("label", [
                  _vm._v("Search:\n                      "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.tableData.search,
                        expression: "tableData.search"
                      }
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "",
                      "aria-controls": "tableData"
                    },
                    domProps: { value: _vm.tableData.search },
                    on: {
                      input: [
                        function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.tableData, "search", $event.target.value)
                        },
                        function($event) {
                          return _vm.loadData()
                        }
                      ]
                    }
                  })
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "datatable",
              {
                attrs: {
                  columns: _vm.columns,
                  sortKey: _vm.sortKey,
                  sortOrders: _vm.sortOrders
                },
                on: { sort: _vm.sortBy }
              },
              [
                _vm.dataTables.length != 0
                  ? _c(
                      "tbody",
                      _vm._l(_vm.dataTables, function(dataTable, index) {
                        return _c("tr", { key: index }, [
                          _c("td", [_vm._v(_vm._s(index + 1))]),
                          _vm._v(" "),
                          dataTable.status_confirmation == 1
                            ? _c("td", [
                                _vm._v(
                                  "\n                              " +
                                    _vm._s(
                                      _vm._f("censored")(dataTable.number)
                                    ) +
                                    "\n                          "
                                )
                              ])
                            : _c("td", [
                                _vm._v(
                                  "\n                              " +
                                    _vm._s(dataTable.number) +
                                    "\n                          "
                                )
                              ]),
                          _vm._v(" "),
                          dataTable.type == 1
                            ? _c("td", [
                                _vm._v(
                                  "\n                              Hanya Jam\n                          "
                                )
                              ])
                            : _c("td", [
                                _vm._v(
                                  "\n                              Informatif\n                          "
                                )
                              ]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(dataTable.otp))]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(
                                _vm._f("formatDateTime")(dataTable.otp_valid)
                              )
                            )
                          ]),
                          _vm._v(" "),
                          dataTable.status_confirmation == 1
                            ? _c("td", [
                                _c(
                                  "span",
                                  { staticClass: "badge badge-warning" },
                                  [_vm._v("Menunggu Persetujuan")]
                                )
                              ])
                            : _c("td", [
                                _c(
                                  "span",
                                  { staticClass: "badge badge-success" },
                                  [_vm._v("Terverifikasi")]
                                )
                              ]),
                          _vm._v(" "),
                          dataTable.application_version >=
                          _vm.globalVar.session.mosque.version
                            ? _c("td", [
                                _c(
                                  "span",
                                  { staticClass: "badge badge-success" },
                                  [_vm._v("Terbaru")]
                                )
                              ])
                            : _c("td", [
                                _c(
                                  "span",
                                  {
                                    staticClass: "badge badge-warning pointer",
                                    on: { click: _vm.showUpdateAlert }
                                  },
                                  [_vm._v("Butuh Pembaruan")]
                                )
                              ]),
                          _vm._v(" "),
                          _c("td", {
                            domProps: {
                              innerHTML: _vm._s(
                                _vm.statusLabel(dataTable.status)
                              )
                            }
                          }),
                          _vm._v(" "),
                          _c("td", { staticClass: "text-center" }, [
                            dataTable.status_confirmation == 1
                              ? _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-success btn-sm",
                                    attrs: {
                                      type: "button",
                                      title: "Verifikasi"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.openModalVerification(
                                          dataTable
                                        )
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-check-circle"
                                    })
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            dataTable.status_confirmation == 2
                              ? _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-success btn-sm",
                                    attrs: {
                                      type: "button",
                                      title: "Generate Ulang OTP"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.generateOtp(dataTable)
                                      }
                                    }
                                  },
                                  [_c("i", { staticClass: "fas fa-sync-alt" })]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            dataTable.status_confirmation == 2 &&
                            dataTable.status == 1
                              ? _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-danger btn-sm",
                                    attrs: {
                                      type: "button",
                                      title: "Non Aktifkan"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.activeData(dataTable)
                                      }
                                    }
                                  },
                                  [_c("i", { staticClass: "fas fa-ban" })]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            dataTable.status_confirmation == 2 &&
                            dataTable.status == 0
                              ? _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-success btn-sm",
                                    attrs: {
                                      type: "button",
                                      title: "Aktifkan"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.activeData(dataTable)
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-check-circle"
                                    })
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            dataTable.status_confirmation == 2
                              ? _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-primary btn-sm",
                                    attrs: {
                                      type: "button",
                                      title: "Ubah Data"
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.openModal(dataTable)
                                      }
                                    }
                                  },
                                  [_c("i", { staticClass: "fas fa-edit" })]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-danger btn-sm",
                                attrs: { type: "button", title: "Hapus Data" },
                                on: {
                                  click: function($event) {
                                    return _vm.deleteData(dataTable)
                                  }
                                }
                              },
                              [_c("i", { staticClass: "fas fa-trash-alt" })]
                            )
                          ])
                        ])
                      }),
                      0
                    )
                  : _c("tbody", [
                      _c("tr", [
                        _c(
                          "td",
                          {
                            staticClass: "text-center",
                            attrs: { colspan: "9" }
                          },
                          [
                            _vm._v(
                              "\n                          Tidak ada data yang cocok ditemukan\n                      "
                            )
                          ]
                        )
                      ])
                    ])
              ]
            ),
            _vm._v(" "),
            _c("pagination", {
              attrs: { pagination: _vm.pagination },
              on: {
                prev: function($event) {
                  return _vm.loadData(_vm.pagination.prevPageUrl)
                },
                next: function($event) {
                  return _vm.loadData(_vm.pagination.nextPageUrl)
                }
              }
            })
          ],
          1
        )
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade bd-example-modal-lg",
        attrs: {
          id: "modalFormVerification",
          role: "dialog",
          "aria-labelledby": "modalFormVerificationLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog modal-lg", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(0),
              _vm._v(" "),
              _c(
                "form",
                {
                  attrs: { id: "formAddVerification" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.submitDataVerification($event)
                    }
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "modal-body" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.formVerification.id,
                            expression: "formVerification.id"
                          }
                        ],
                        staticClass: "d-none",
                        class: {
                          "is-invalid": _vm.formVerification.errors.has("id")
                        },
                        attrs: {
                          type: "text",
                          name: "id",
                          id: "id_verification"
                        },
                        domProps: { value: _vm.formVerification.id },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.formVerification,
                              "id",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.formVerification, field: "id" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "number_verification" }
                          },
                          [
                            _vm._v(
                              "\n                      Id Media TV\n                  "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formVerification.number,
                                  expression: "formVerification.number"
                                }
                              ],
                              ref: "number_verification",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.formVerification.errors.has(
                                  "number"
                                )
                              },
                              attrs: {
                                type: "text",
                                name: "number",
                                id: "number_verification",
                                placeholder: "Id Media TV",
                                autocomplete: "off",
                                required: ""
                              },
                              domProps: { value: _vm.formVerification.number },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.formVerification,
                                    "number",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: {
                                form: _vm.formVerification,
                                field: "number"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "type" }
                          },
                          [
                            _vm._v(
                              "\n                    Tipe\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            key: _vm.renderingCountType2,
                            staticClass: "col-sm-8"
                          },
                          [
                            _c("select2", {
                              ref: "type2",
                              attrs: {
                                url: "",
                                name: "type2",
                                selecteditems: _vm.selecteditemsType2,
                                identifier: "type2",
                                placeholder: "Tipe",
                                required: ""
                              },
                              on: { changed: _vm.changeType2 }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: {
                                form: _vm.formVerification,
                                field: "type"
                              }
                            })
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm._m(1)
                ]
              )
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade bd-example-modal-lg",
        attrs: {
          id: "modalForm",
          role: "dialog",
          "aria-labelledby": "modalFormLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog modal-lg", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(2),
              _vm._v(" "),
              _c(
                "form",
                {
                  attrs: { id: "formAdd" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.submitData($event)
                    }
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "modal-body" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.id,
                            expression: "form.id"
                          }
                        ],
                        staticClass: "d-none",
                        class: { "is-invalid": _vm.form.errors.has("id") },
                        attrs: { type: "text", name: "id", id: "id" },
                        domProps: { value: _vm.form.id },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "id", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "id" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "number" }
                          },
                          [
                            _vm._v(
                              "\n                      Id Media TV\n                  "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.number,
                                  expression: "form.number"
                                }
                              ],
                              ref: "number",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("number")
                              },
                              attrs: {
                                type: "text",
                                name: "number",
                                id: "number",
                                placeholder: "Id Media TV",
                                autocomplete: "off",
                                required: "",
                                readonly: _vm.form.id ? true : false
                              },
                              domProps: { value: _vm.form.number },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "number",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "number" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "name" }
                          },
                          [
                            _vm._v(
                              "\n                    Nama\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.name,
                                  expression: "form.name"
                                }
                              ],
                              ref: "name",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("name")
                              },
                              attrs: {
                                type: "text",
                                name: "name",
                                id: "name",
                                placeholder: "Nama",
                                autocomplete: "off",
                                required: ""
                              },
                              domProps: { value: _vm.form.name },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "name",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "name" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "type" }
                          },
                          [
                            _vm._v(
                              "\n                    Tipe\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            key: _vm.renderingCountType,
                            staticClass: "col-sm-8"
                          },
                          [
                            _c("select2", {
                              ref: "type",
                              attrs: {
                                url: "",
                                name: "type",
                                selecteditems: _vm.selecteditemsType,
                                identifier: "type",
                                placeholder: "Tipe",
                                required: ""
                              },
                              on: { changed: _vm.changeType }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "type" }
                            })
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm._m(3)
                ]
              )
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        {
          staticClass: "modal-title",
          attrs: { id: "modalFormVerificationLabel" }
        },
        [_vm._v("\n              Modal Form\n          ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer text-right" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n                  Batal\n              ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-success", attrs: { type: "submit" } },
        [_vm._v("\n                  Simpan\n              ")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        { staticClass: "modal-title", attrs: { id: "modalFormLabel" } },
        [_vm._v("\n              Modal Form\n          ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer text-right" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n                  Batal\n              ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-success", attrs: { type: "submit" } },
        [_vm._v("\n                  Simpan\n              ")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/Step4.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/backoffice/Step4.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Step4_vue_vue_type_template_id_4e9ebc67___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Step4.vue?vue&type=template&id=4e9ebc67& */ "./resources/js/components/backoffice/Step4.vue?vue&type=template&id=4e9ebc67&");
/* harmony import */ var _Step4_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Step4.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/Step4.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Step4_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Step4_vue_vue_type_template_id_4e9ebc67___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Step4_vue_vue_type_template_id_4e9ebc67___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/Step4.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/Step4.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/backoffice/Step4.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Step4_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Step4.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step4.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Step4_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/Step4.vue?vue&type=template&id=4e9ebc67&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/backoffice/Step4.vue?vue&type=template&id=4e9ebc67& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step4_vue_vue_type_template_id_4e9ebc67___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Step4.vue?vue&type=template&id=4e9ebc67& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step4.vue?vue&type=template&id=4e9ebc67&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step4_vue_vue_type_template_id_4e9ebc67___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step4_vue_vue_type_template_id_4e9ebc67___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);