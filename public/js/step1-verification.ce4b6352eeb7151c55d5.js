(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/step1-verification"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step1.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Step1.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    this.loadProvince();
    Fire.$on('AfterLoaded', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      renderingCountProvince: 0,
      form: new Form({
        id: '',
        number: '',
        logo: '',
        name: '',
        province_id: 0,
        address: '',
        latitude: '',
        longitude: '',
        show_ads: '',
        show_name: '',
        show_logo: '',
        show_address: ''
      }),
      selecteditemsProvince: [],
      logo_default: this.$attrs.globalVar.apiStorage + 'images/default.svg'
    };
  },
  components: {},
  methods: {
    loadProvince: function loadProvince() {
      var self = this;
      self.deleteDefaultHeaders();
      axios.get("".concat(self.globalVar.apiUrl, "province/select"), {
        headers: self.globalVar.headers
      }).then(function (response) {
        self.selecteditemsProvince = [];

        for (var i = 0; i < response.data.length; i++) {
          var provinceData = {
            id: response.data[i].id,
            name: response.data[i].name
          };
          self.selecteditemsProvince.push(provinceData);
        }

        self.renderingCountProvince++;
        Fire.$emit('AfterLoaded');
      })["catch"](function (error) {
        if (error.response) {}
      });
    },
    changeProvince: function changeProvince() {
      var self = this;
      var provinceId = 0;

      if ($("#province_id").select2('data')[0]) {
        provinceId = $("#province_id").select2('data')[0].id;
      }

      self.form.province_id = parseInt(provinceId);
    },
    loadData: function loadData() {
      if (this.$attrs.globalVar) {
        if (this.$attrs.globalVar.session) {
          if (this.$attrs.globalVar.session.mosque) {
            this.form.id = this.$attrs.globalVar.session.mosque.id;
            this.form.number = this.$attrs.globalVar.session.mosque.number;
            this.form.logo = this.$attrs.globalVar.session.mosque.logo;
            this.logo_default = this.$attrs.globalVar.session.mosque.logo;
            this.form.name = this.$attrs.globalVar.session.mosque.name;
            this.form.address = this.$attrs.globalVar.session.mosque.address;
            this.form.latitude = this.$attrs.globalVar.session.mosque.latitude;
            this.form.longitude = this.$attrs.globalVar.session.mosque.longitude;
            this.form.show_ads = this.$attrs.globalVar.session.mosque.show_ads;
            this.form.show_name = this.$attrs.globalVar.session.mosque.show_name;
            this.form.show_address = this.$attrs.globalVar.session.mosque.show_address;
            this.form.show_logo = this.$attrs.globalVar.session.mosque.show_logo;

            if (this.$attrs.globalVar.session.mosque.province) {
              this.form.province_id = this.$attrs.globalVar.session.mosque.province.id;

              for (var i = 0; i < this.selecteditemsProvince.length; i++) {
                if (this.selecteditemsProvince[i].id == this.$attrs.globalVar.session.mosque.province.id) {
                  setTimeout(function () {
                    if (document.getElementById("province_id")) {
                      $("#province_id").select2("destroy");
                      document.getElementById("province_id").selectedIndex = i;
                      $("#province_id").select2({
                        width: '100%'
                      });
                    }
                  }, 400);
                  break;
                }
              }
            }
          }
        }
      }
    },
    handleImageUpload: function handleImageUpload() {
      var self = this;
      var file = self.$refs.logo.files[0];

      if (file) {
        var reader = new FileReader();

        reader.onload = function (e) {
          self.logo_default = e.target.result;
        };

        reader.readAsDataURL(file);
      }
    },
    submitData: function submitData() {
      var self = this;
      self.$Progress.start();

      if (self.globalVar.statusVerification == -4) {
        Swal.fire({
          html: 'Mengunduh data waktu solat . . . <br><br><div class="progress"><div id="progress" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div></div>',
          onBeforeOpen: function onBeforeOpen() {
            Swal.showLoading();
          }
        });
      } else {
        self.showProgressBar();
      }

      this.deleteDefaultHeaders();
      self.form.latitude = '';
      self.form.longitude = '';
      axios.get("https://maps.googleapis.com/maps/api/geocode/json", {
        params: {
          'address': self.form.address,
          'key': self.globalVar.googleMapKey
        }
      }).then(function (response) {
        if (response.data.results.length > 0) {
          self.form.latitude = response.data.results[0].geometry.location.lat;
          self.form.longitude = response.data.results[0].geometry.location.lng;
          document.getElementsByName('latitude')[0].value = response.data.results[0].geometry.location.lat;
          document.getElementsByName('longitude')[0].value = response.data.results[0].geometry.location.lng;
          self.$Progress.finish();
          var form = document.getElementById('formAdd');
          var formData = new FormData(form);
          axios.post("".concat(self.globalVar.apiUrl, "mosque/").concat(self.form.id), formData, {
            headers: {
              'Authorization': localStorage.getItem('token'),
              'Content-Type': 'multipart/form-data'
            },
            onUploadProgress: function onUploadProgress(progressEvent) {
              var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100));
              $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
            }
          }).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data updated successfully'
            });
            self.$Progress.finish();
            self.checkAuth();
          })["catch"](function (error) {
            var message = 'Something went wrong.';

            if (error.response) {
              message = error.response.data.message;
              Swal.fire('Alert!', message, 'warning');
            }

            self.$Progress.fail();
          });
        } else {
          var message = 'Alamat tidak valid.';
          Swal.fire('Alert!', message, 'warning');
          self.$Progress.fail();
        }
      })["catch"](function (error) {});
    }
  },
  watch: {},
  activated: function activated() {
    var self = this;
    self.selecteditemsProvince = [];
    self.renderingCountProvince++;
    self.loadProvince();
    Fire.$emit('AfterLoaded');
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step1.vue?vue&type=template&id=4e7475e4&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Step1.vue?vue&type=template&id=4e7475e4& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row p-3" }, [
      _c("div", { staticClass: "col-12" }, [
        _c(
          "form",
          {
            attrs: { id: "formAdd" },
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.submitData($event)
              }
            }
          },
          [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.id,
                  expression: "form.id"
                }
              ],
              staticClass: "d-none",
              class: { "is-invalid": _vm.form.errors.has("id") },
              attrs: { type: "text", name: "id", id: "id" },
              domProps: { value: _vm.form.id },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "id", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("has-error", { attrs: { form: _vm.form, field: "id" } }),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-6 col-xs-12" }, [
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "number" }
                    },
                    [
                      _vm._v(
                        "\n                              Id Masjid\n                          "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.number,
                            expression: "form.number"
                          }
                        ],
                        ref: "number",
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.form.errors.has("number") },
                        attrs: {
                          type: "text",
                          name: "number",
                          id: "number",
                          placeholder: "Id Masjid",
                          autocomplete: "off",
                          readonly: ""
                        },
                        domProps: { value: _vm.form.number },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "number", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "number" }
                      }),
                      _vm._v(" "),
                      _c("small", { staticClass: "form-text text-muted" }, [
                        _vm._v(
                          "\n                                  *) Otomatis dari sistem.\n                              "
                        )
                      ])
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "name" }
                    },
                    [
                      _vm._v(
                        "\n                              Nama Masjid\n                          "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.name,
                            expression: "form.name"
                          }
                        ],
                        ref: "name",
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.form.errors.has("name") },
                        attrs: {
                          type: "text",
                          name: "name",
                          id: "name",
                          placeholder: "Nama Masjid",
                          autocomplete: "off",
                          required: ""
                        },
                        domProps: { value: _vm.form.name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "name", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "name" }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "show_name" }
                    },
                    [
                      _vm._v(
                        "\n                          Tampilkan Nama\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c(
                        "div",
                        { staticClass: "custom-control custom-switch pointer" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.show_name,
                                expression: "form.show_name"
                              }
                            ],
                            ref: "show_name",
                            staticClass: "custom-control-input",
                            class: {
                              "is-invalid": _vm.form.errors.has("show_name")
                            },
                            attrs: {
                              type: "checkbox",
                              id: "show_name",
                              name: "show_name"
                            },
                            domProps: {
                              checked: Array.isArray(_vm.form.show_name)
                                ? _vm._i(_vm.form.show_name, null) > -1
                                : _vm.form.show_name
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.form.show_name,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        _vm.form,
                                        "show_name",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        _vm.form,
                                        "show_name",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(_vm.form, "show_name", $$c)
                                }
                              }
                            }
                          }),
                          _vm._v(" "),
                          !_vm.form.show_name
                            ? _c(
                                "label",
                                {
                                  staticClass: "custom-control-label",
                                  attrs: { for: "show_name" }
                                },
                                [_vm._v("Tidak Aktif")]
                              )
                            : _c(
                                "label",
                                {
                                  staticClass: "custom-control-label",
                                  attrs: { for: "show_name" }
                                },
                                [_vm._v("Aktif")]
                              )
                        ]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "show_name" }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "province_id" }
                    },
                    [
                      _vm._v(
                        "\n                              Provinsi\n                          "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      key: _vm.renderingCountProvince,
                      staticClass: "col-sm-8"
                    },
                    [
                      _c("select2", {
                        ref: "province_id",
                        attrs: {
                          url: "",
                          name: "province_id",
                          selecteditems: _vm.selecteditemsProvince,
                          identifier: "province_id",
                          placeholder: "Provinsi",
                          required: ""
                        },
                        on: { changed: _vm.changeProvince }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "address" }
                    },
                    [
                      _vm._v(
                        "\n                            Alamat\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.address,
                            expression: "form.address"
                          }
                        ],
                        ref: "address",
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.form.errors.has("address") },
                        attrs: {
                          name: "address",
                          rows: "4",
                          id: "address",
                          placeholder: "Alamat",
                          autocomplete: "off",
                          required: ""
                        },
                        domProps: { value: _vm.form.address },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "address", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "address" }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "show_address" }
                    },
                    [
                      _vm._v(
                        "\n                          Tampilkan Alamat\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c(
                        "div",
                        { staticClass: "custom-control custom-switch pointer" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.show_address,
                                expression: "form.show_address"
                              }
                            ],
                            ref: "show_address",
                            staticClass: "custom-control-input",
                            class: {
                              "is-invalid": _vm.form.errors.has("show_address")
                            },
                            attrs: {
                              type: "checkbox",
                              id: "show_address",
                              name: "show_address"
                            },
                            domProps: {
                              checked: Array.isArray(_vm.form.show_address)
                                ? _vm._i(_vm.form.show_address, null) > -1
                                : _vm.form.show_address
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.form.show_address,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        _vm.form,
                                        "show_address",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        _vm.form,
                                        "show_address",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(_vm.form, "show_address", $$c)
                                }
                              }
                            }
                          }),
                          _vm._v(" "),
                          !_vm.form.show_address
                            ? _c(
                                "label",
                                {
                                  staticClass: "custom-control-label",
                                  attrs: { for: "show_address" }
                                },
                                [_vm._v("Tidak Aktif")]
                              )
                            : _c(
                                "label",
                                {
                                  staticClass: "custom-control-label",
                                  attrs: { for: "show_address" }
                                },
                                [_vm._v("Aktif")]
                              )
                        ]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "show_address" }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "latitude" }
                    },
                    [
                      _vm._v(
                        "\n                            Latitude\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.latitude,
                            expression: "form.latitude"
                          }
                        ],
                        ref: "latitude",
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.form.errors.has("latitude")
                        },
                        attrs: {
                          type: "text",
                          name: "latitude",
                          id: "latitude",
                          placeholder: "Latitude",
                          autocomplete: "off",
                          required: ""
                        },
                        domProps: { value: _vm.form.latitude },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "latitude", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "latitude" }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "longitude" }
                    },
                    [
                      _vm._v(
                        "\n                            Longitude\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.longitude,
                            expression: "form.longitude"
                          }
                        ],
                        ref: "longitude",
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.form.errors.has("longitude")
                        },
                        attrs: {
                          type: "text",
                          name: "longitude",
                          id: "longitude",
                          placeholder: "Longitude",
                          autocomplete: "off",
                          required: ""
                        },
                        domProps: { value: _vm.form.longitude },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "longitude", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "longitude" }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "show_ads" }
                    },
                    [
                      _vm._v(
                        "\n                            Tampilkan Iklan\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c(
                        "div",
                        { staticClass: "custom-control custom-switch pointer" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.show_ads,
                                expression: "form.show_ads"
                              }
                            ],
                            ref: "show_ads",
                            staticClass: "custom-control-input",
                            class: {
                              "is-invalid": _vm.form.errors.has("show_ads")
                            },
                            attrs: {
                              type: "checkbox",
                              id: "show_ads",
                              name: "show_ads"
                            },
                            domProps: {
                              checked: Array.isArray(_vm.form.show_ads)
                                ? _vm._i(_vm.form.show_ads, null) > -1
                                : _vm.form.show_ads
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.form.show_ads,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        _vm.form,
                                        "show_ads",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        _vm.form,
                                        "show_ads",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(_vm.form, "show_ads", $$c)
                                }
                              }
                            }
                          }),
                          _vm._v(" "),
                          !_vm.form.show_ads
                            ? _c(
                                "label",
                                {
                                  staticClass: "custom-control-label",
                                  attrs: { for: "show_ads" }
                                },
                                [_vm._v("Tidak Aktif")]
                              )
                            : _c(
                                "label",
                                {
                                  staticClass: "custom-control-label",
                                  attrs: { for: "show_ads" }
                                },
                                [_vm._v("Aktif")]
                              )
                        ]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "show_ads" }
                      })
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6 col-xs-12" }, [
                _c("div", { staticClass: "form-group row" }, [
                  _c("div", { staticClass: "col-sm-12 text-center" }, [
                    _c("img", {
                      staticClass: "img-fluid",
                      staticStyle: { width: "40%" },
                      attrs: { id: "logo_preview", src: _vm.logo_default }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "name" }
                    },
                    [
                      _vm._v(
                        "\n                            Logo\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-8" }, [
                    _c("div", { staticClass: "custom-file" }, [
                      _c("input", {
                        ref: "logo",
                        staticClass: "custom-file-input",
                        attrs: {
                          type: "file",
                          name: "logo",
                          id: "logo",
                          placeholder: "Gambar",
                          autocomplete: "off",
                          required: _vm.form.id ? false : true
                        },
                        on: { change: _vm.handleImageUpload }
                      }),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "custom-file-label",
                          attrs: { for: "logo" }
                        },
                        [_vm._v("Choose file")]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "text-justify mt-3" }, [
                        _vm.form.id
                          ? _c(
                              "small",
                              { staticClass: "form-text text-muted" },
                              [
                                _vm._v(
                                  "\n                                        Kosongkan jika tidak diganti.\n                                    "
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("small", { staticClass: "form-text text-muted" }, [
                          _vm._v(
                            "\n                                        File tipe hanya jpeg, png, bmp, gif, svg, atau webp\n                                    "
                          )
                        ]),
                        _vm._v(" "),
                        _c("small", { staticClass: "form-text text-muted" }, [
                          _vm._v(
                            "\n                                        Ukuran maksimal 1024Kb\n                                    "
                          )
                        ])
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group row" }, [
                  _c(
                    "label",
                    {
                      staticClass: "col-sm-4 col-form-label",
                      attrs: { for: "show_logo" }
                    },
                    [
                      _vm._v(
                        "\n                              Tampilkan Logo\n                          "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-8" },
                    [
                      _c(
                        "div",
                        { staticClass: "custom-control custom-switch pointer" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.show_logo,
                                expression: "form.show_logo"
                              }
                            ],
                            ref: "show_logo",
                            staticClass: "custom-control-input",
                            class: {
                              "is-invalid": _vm.form.errors.has("show_logo")
                            },
                            attrs: {
                              type: "checkbox",
                              id: "show_logo",
                              name: "show_logo"
                            },
                            domProps: {
                              checked: Array.isArray(_vm.form.show_logo)
                                ? _vm._i(_vm.form.show_logo, null) > -1
                                : _vm.form.show_logo
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.form.show_logo,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        _vm.form,
                                        "show_logo",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        _vm.form,
                                        "show_logo",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(_vm.form, "show_logo", $$c)
                                }
                              }
                            }
                          }),
                          _vm._v(" "),
                          !_vm.form.show_logo
                            ? _c(
                                "label",
                                {
                                  staticClass: "custom-control-label",
                                  attrs: { for: "show_logo" }
                                },
                                [_vm._v("Tidak Aktif")]
                              )
                            : _c(
                                "label",
                                {
                                  staticClass: "custom-control-label",
                                  attrs: { for: "show_logo" }
                                },
                                [_vm._v("Aktif")]
                              )
                        ]
                      ),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "show_ads" }
                      })
                    ],
                    1
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _vm._m(0)
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-right" }, [
        _c(
          "button",
          { staticClass: "btn btn-success", attrs: { type: "submit" } },
          [_vm._v("\n                      Simpan\n                  ")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/Step1.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/backoffice/Step1.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Step1_vue_vue_type_template_id_4e7475e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Step1.vue?vue&type=template&id=4e7475e4& */ "./resources/js/components/backoffice/Step1.vue?vue&type=template&id=4e7475e4&");
/* harmony import */ var _Step1_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Step1.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/Step1.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Step1_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Step1_vue_vue_type_template_id_4e7475e4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Step1_vue_vue_type_template_id_4e7475e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/Step1.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/Step1.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/backoffice/Step1.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Step1_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Step1.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step1.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Step1_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/Step1.vue?vue&type=template&id=4e7475e4&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/backoffice/Step1.vue?vue&type=template&id=4e7475e4& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step1_vue_vue_type_template_id_4e7475e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Step1.vue?vue&type=template&id=4e7475e4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step1.vue?vue&type=template&id=4e7475e4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step1_vue_vue_type_template_id_4e7475e4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step1_vue_vue_type_template_id_4e7475e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);