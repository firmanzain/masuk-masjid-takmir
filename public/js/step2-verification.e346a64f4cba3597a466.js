(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/step2-verification"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step2.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Step2.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {},
  mounted: function mounted() {},
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      form: new Form({
        id: this.$attrs.globalVar.session.id,
        name: this.$attrs.globalVar.session.name,
        username: this.$attrs.globalVar.session.username,
        password: ''
      }),
      showPassword: false
    };
  },
  components: {},
  methods: {
    submitData: function submitData() {
      var _this = this;

      var self = this;
      self.$Progress.start();
      this.deleteDefaultHeaders();
      var method = '';

      if (self.form.id) {
        method = axios.put("".concat(self.globalVar.apiUrl, "takmir/").concat(self.form.id), self.form, {
          headers: self.globalVar.headers
        });
      } else {
        method = axios.post("".concat(self.globalVar.apiUrl, "takmir"), self.form, {
          headers: self.globalVar.headers
        });
      }

      method.then(function (response) {
        Toast.fire({
          type: 'success',
          title: 'Data updated successfully'
        });
        self.$Progress.finish();
        var formData = new FormData();
        axios.post("".concat(self.globalVar.apiUrl, "mosque-status/").concat(self.globalVar.session.mosque.id), formData, {
          headers: {
            'Authorization': localStorage.getItem('token'),
            'Content-Type': 'application/json'
          }
        }).then(function (response) {
          _this.checkAuth();
        })["catch"](function (error) {});
      })["catch"](function (error) {
        var message = 'Something went wrong.';

        if (error.response) {
          message = error.response.data.message;
          Swal.fire('Alert!', message, 'warning');
        }

        self.$Progress.fail();
      });
    },
    changeShowPasswordStatus: function changeShowPasswordStatus() {
      if (this.showPassword) {
        this.showPassword = false;
      } else {
        this.showPassword = true;
      }
    }
  },
  watch: {},
  activated: function activated() {
    var self = this;
    self.showPassword = false;
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step2.vue?vue&type=template&id=4e828d65&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Step2.vue?vue&type=template&id=4e828d65& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row p-3" }, [
      _c("div", { staticClass: "col-12" }, [
        _c(
          "form",
          {
            attrs: { id: "formAdd" },
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.submitData($event)
              }
            }
          },
          [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.id,
                  expression: "form.id"
                }
              ],
              staticClass: "d-none",
              class: { "is-invalid": _vm.form.errors.has("id") },
              attrs: { type: "text", name: "id", id: "id" },
              domProps: { value: _vm.form.id },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "id", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("has-error", { attrs: { form: _vm.form, field: "id" } }),
            _vm._v(" "),
            _c("div", { staticClass: "form-group row" }, [
              _c(
                "label",
                {
                  staticClass: "col-sm-4 col-form-label",
                  attrs: { for: "name" }
                },
                [_vm._v("\n                        Nama\n                    ")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-sm-8" },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.name,
                        expression: "form.name"
                      }
                    ],
                    ref: "name",
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("name") },
                    attrs: {
                      type: "text",
                      name: "name",
                      id: "name",
                      placeholder: "Nama",
                      autocomplete: "off",
                      required: ""
                    },
                    domProps: { value: _vm.form.name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "name", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("has-error", { attrs: { form: _vm.form, field: "name" } })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group row" }, [
              _c(
                "label",
                {
                  staticClass: "col-sm-4 col-form-label",
                  attrs: { for: "username" }
                },
                [
                  _vm._v(
                    "\n                        Username\n                    "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-sm-8" },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.username,
                        expression: "form.username"
                      }
                    ],
                    ref: "username",
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("username") },
                    attrs: {
                      type: "text",
                      name: "username",
                      id: "username",
                      placeholder: "Username",
                      autocomplete: "off",
                      required: "",
                      readonly: _vm.form.id ? true : false
                    },
                    domProps: { value: _vm.form.username },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "username", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("has-error", {
                    attrs: { form: _vm.form, field: "username" }
                  })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group row" }, [
              _c(
                "label",
                {
                  staticClass: "col-sm-4 col-form-label",
                  attrs: { for: "username" }
                },
                [
                  _vm._v(
                    "\n                        Password\n                    "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-sm-8" },
                [
                  _c("div", { staticClass: "input-group" }, [
                    (_vm.showPassword ? "text" : "password") === "checkbox"
                      ? _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.password,
                              expression: "form.password"
                            }
                          ],
                          ref: "password",
                          staticClass: "form-control",
                          class: {
                            "is-invalid": _vm.form.errors.has("password")
                          },
                          attrs: {
                            name: "password",
                            id: "password",
                            placeholder: "Password",
                            autocomplete: "off",
                            required: _vm.form.id ? false : true,
                            type: "checkbox"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.form.password)
                              ? _vm._i(_vm.form.password, null) > -1
                              : _vm.form.password
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.form.password,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.form,
                                      "password",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.form,
                                      "password",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.form, "password", $$c)
                              }
                            }
                          }
                        })
                      : (_vm.showPassword ? "text" : "password") === "radio"
                      ? _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.password,
                              expression: "form.password"
                            }
                          ],
                          ref: "password",
                          staticClass: "form-control",
                          class: {
                            "is-invalid": _vm.form.errors.has("password")
                          },
                          attrs: {
                            name: "password",
                            id: "password",
                            placeholder: "Password",
                            autocomplete: "off",
                            required: _vm.form.id ? false : true,
                            type: "radio"
                          },
                          domProps: {
                            checked: _vm._q(_vm.form.password, null)
                          },
                          on: {
                            change: function($event) {
                              return _vm.$set(_vm.form, "password", null)
                            }
                          }
                        })
                      : _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.password,
                              expression: "form.password"
                            }
                          ],
                          ref: "password",
                          staticClass: "form-control",
                          class: {
                            "is-invalid": _vm.form.errors.has("password")
                          },
                          attrs: {
                            name: "password",
                            id: "password",
                            placeholder: "Password",
                            autocomplete: "off",
                            required: _vm.form.id ? false : true,
                            type: _vm.showPassword ? "text" : "password"
                          },
                          domProps: { value: _vm.form.password },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.form,
                                "password",
                                $event.target.value
                              )
                            }
                          }
                        }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "input-group-append",
                        staticStyle: { cursor: "pointer" },
                        on: { click: _vm.changeShowPasswordStatus }
                      },
                      [
                        _c("span", { staticClass: "input-group-text" }, [
                          !_vm.showPassword
                            ? _c("i", { staticClass: "fas fa-eye-slash" })
                            : _c("i", { staticClass: "fas fa-eye" })
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _vm.form.id
                    ? _c("small", { staticClass: "form-text text-muted" }, [
                        _vm._v(
                          "\n                            Kosongkan jika tidak diganti.\n                        "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("has-error", {
                    attrs: { form: _vm.form, field: "password" }
                  })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _vm._m(0)
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-right" }, [
        _c(
          "button",
          { staticClass: "btn btn-success", attrs: { type: "submit" } },
          [
            _vm._v(
              "\n                            Simpan\n                        "
            )
          ]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/Step2.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/backoffice/Step2.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Step2_vue_vue_type_template_id_4e828d65___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Step2.vue?vue&type=template&id=4e828d65& */ "./resources/js/components/backoffice/Step2.vue?vue&type=template&id=4e828d65&");
/* harmony import */ var _Step2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Step2.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/Step2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Step2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Step2_vue_vue_type_template_id_4e828d65___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Step2_vue_vue_type_template_id_4e828d65___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/Step2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/Step2.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/backoffice/Step2.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Step2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Step2.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Step2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/Step2.vue?vue&type=template&id=4e828d65&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/backoffice/Step2.vue?vue&type=template&id=4e828d65& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step2_vue_vue_type_template_id_4e828d65___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Step2.vue?vue&type=template&id=4e828d65& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Step2.vue?vue&type=template&id=4e828d65&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step2_vue_vue_type_template_id_4e828d65___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Step2_vue_vue_type_template_id_4e828d65___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);