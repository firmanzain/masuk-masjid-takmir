(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/verification"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Verification.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Verification.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var Step1 = function Step1() {
  return __webpack_require__.e(/*! import() | js/step1-verification */ "js/step1-verification").then(__webpack_require__.bind(null, /*! ./Step1.vue */ "./resources/js/components/backoffice/Step1.vue"));
};

var Step2 = function Step2() {
  return __webpack_require__.e(/*! import() | js/step2-verification */ "js/step2-verification").then(__webpack_require__.bind(null, /*! ./Step2.vue */ "./resources/js/components/backoffice/Step2.vue"));
};

var Step3 = function Step3() {
  return __webpack_require__.e(/*! import() | js/step3-verification */ "js/step3-verification").then(__webpack_require__.bind(null, /*! ./Step3.vue */ "./resources/js/components/backoffice/Step3.vue"));
};

var Step4 = function Step4() {
  return __webpack_require__.e(/*! import() | js/step4-verification */ "js/step4-verification").then(__webpack_require__.bind(null, /*! ./Step4.vue */ "./resources/js/components/backoffice/Step4.vue"));
};

var date = new Date();
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {},
  mounted: function mounted() {},
  components: {
    Step1: Step1,
    Step2: Step2,
    Step3: Step3,
    Step4: Step4
  },
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      component: 'Step1'
    };
  },
  methods: {
    changedStepPrev: function changedStepPrev() {
      var self = this;

      if (self.component == 'Step2') {
        self.component = 'Step1';
      } else if (self.component == 'Step3') {
        self.component = 'Step2';
      } else if (self.component == 'Step4') {
        self.component = 'Step3';
      }
    },
    changedStepNext: function changedStepNext() {
      var self = this;
      var changedStatus = false;

      if (self.component == 'Step1') {
        if (self.globalVar.statusVerification == -4) {
          changedStatus = true;
        }

        self.component = 'Step2';
      } else if (self.component == 'Step2') {
        if (self.globalVar.statusVerification == -3) {
          changedStatus = true;
        }

        self.component = 'Step3';
      } else if (self.component == 'Step3') {
        if (self.globalVar.statusVerification == -2) {
          changedStatus = true;
        }

        self.component = 'Step4';
      } else {
        changedStatus = true;
      }

      if (changedStatus) {
        var formData = new FormData();
        axios.post("".concat(self.globalVar.apiUrl, "mosque-status/").concat(self.globalVar.session.mosque.id), formData, {
          headers: {
            'Authorization': localStorage.getItem('token'),
            'Content-Type': 'application/json'
          }
        }).then(function (response) {
          self.checkAuth();

          if (response.data.message.status == 1) {
            self.$router.replace('/mosque');
          }
        })["catch"](function (error) {});
      }
    }
  },
  watch: {
    '$attrs.globalVar': function $attrsGlobalVar(to, from) {
      if (to.statusVerification == -4) {
        this.component = 'Step1';
      } else if (to.statusVerification == -3) {
        this.component = 'Step2';
      } else if (to.statusVerification == -2) {
        this.component = 'Step3';
      } else if (to.statusVerification == -1) {
        this.component = 'Step4';
      }
    }
  },
  activated: function activated() {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Verification.vue?vue&type=template&id=72fd5328&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/Verification.vue?vue&type=template&id=72fd5328& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("section", { staticClass: "content pt-3 pb-5" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c(
              "div",
              {
                staticClass: "sw-main sw-theme-arrows",
                attrs: { id: "smartwizard" }
              },
              [
                _c("ul", { staticClass: "nav nav-tabs step-anchor" }, [
                  _c(
                    "li",
                    {
                      ref: "navStep1",
                      staticClass: "nav-item",
                      class:
                        _vm.globalVar.statusVerification >= -4 ? "active" : "",
                      staticStyle: { width: "calc(100% / 4)" }
                    },
                    [_vm._m(0)]
                  ),
                  _vm._v(" "),
                  _c(
                    "li",
                    {
                      ref: "navStep2",
                      staticClass: "nav-item",
                      class:
                        _vm.globalVar.statusVerification >= -3 ? "active" : "",
                      staticStyle: { width: "calc(100% / 4)" }
                    },
                    [_vm._m(1)]
                  ),
                  _vm._v(" "),
                  _c(
                    "li",
                    {
                      ref: "navStep3",
                      staticClass: "nav-item",
                      class:
                        _vm.globalVar.statusVerification >= -2 ? "active" : "",
                      staticStyle: { width: "calc(100% / 4)" }
                    },
                    [_vm._m(2)]
                  ),
                  _vm._v(" "),
                  _c(
                    "li",
                    {
                      ref: "navStep4",
                      staticClass: "nav-item",
                      class:
                        _vm.globalVar.statusVerification >= -1 ? "active" : "",
                      staticStyle: { width: "calc(100% / 4)" }
                    },
                    [_vm._m(3)]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "sw-container tab-content",
                    staticStyle: { "min-height": "166px" }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "tab-pane step-content",
                        staticStyle: { display: "block" }
                      },
                      [
                        _c(
                          "keep-alive",
                          [
                            _c(_vm.component, {
                              tag: "component",
                              attrs: { globalVar: _vm.globalVar }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass:
                      "btn-toolbar sw-toolbar sw-toolbar-bottom justify-content-end"
                  },
                  [
                    _vm.globalVar.statusVerification > -4
                      ? _c(
                          "div",
                          {
                            staticClass: "btn-group mr-2 sw-btn-group",
                            attrs: { role: "group" }
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-secondary sw-btn-prev",
                                class:
                                  _vm.component == "Step1" ? "disabled" : "",
                                attrs: { type: "button" },
                                on: { click: _vm.changedStepPrev }
                              },
                              [
                                _vm._v(
                                  "\n                          Sebelumnya\n                      "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _vm.component != "Step4"
                              ? _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn btn-secondary sw-btn-next",
                                    attrs: { type: "button" },
                                    on: { click: _vm.changedStepNext }
                                  },
                                  [
                                    _vm._v(
                                      "\n                          Selanjutnya\n                      "
                                    )
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.component == "Step4"
                              ? _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-success sw-btn-next",
                                    attrs: { type: "button" },
                                    on: { click: _vm.changedStepNext }
                                  },
                                  [
                                    _vm._v(
                                      "\n                          Selesai\n                      "
                                    )
                                  ]
                                )
                              : _vm._e()
                          ]
                        )
                      : _vm._e()
                  ]
                )
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "nav-link", attrs: { href: "javascript:;" } },
      [
        _vm._v("\n                          Tahap 1"),
        _c("br"),
        _c("b", [_vm._v("Pengisian Informasi Masjid")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "nav-link", attrs: { href: "javascript:;" } },
      [
        _vm._v("\n                          Tahap 2"),
        _c("br"),
        _c("b", [_vm._v("Ubah Password Akun Takmir")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "nav-link", attrs: { href: "javascript:;" } },
      [
        _vm._v("\n                          Tahap 3"),
        _c("br"),
        _c("b", [_vm._v("Penambahan Akun Takmir")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "nav-link", attrs: { href: "javascript:;" } },
      [
        _vm._v("\n                          Tahap 4"),
        _c("br"),
        _c("b", [_vm._v("Halaman Media TV")])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/Verification.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/backoffice/Verification.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Verification_vue_vue_type_template_id_72fd5328___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Verification.vue?vue&type=template&id=72fd5328& */ "./resources/js/components/backoffice/Verification.vue?vue&type=template&id=72fd5328&");
/* harmony import */ var _Verification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Verification.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/Verification.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Verification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Verification_vue_vue_type_template_id_72fd5328___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Verification_vue_vue_type_template_id_72fd5328___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/Verification.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/Verification.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/backoffice/Verification.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Verification.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Verification.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/Verification.vue?vue&type=template&id=72fd5328&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/Verification.vue?vue&type=template&id=72fd5328& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_template_id_72fd5328___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Verification.vue?vue&type=template&id=72fd5328& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/Verification.vue?vue&type=template&id=72fd5328&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_template_id_72fd5328___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_template_id_72fd5328___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);