(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/sidebar-backoffice"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {// $(".has-treeview").click(function(){
    //     $(".has-treeview").not(this).each(function(){
    //         $(this).removeClass("menu-open");
    //         $(this).children("a").first().children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-left");
    //         $(this).children(".nav-treeview").slideUp();
    //     });
    // });
  },
  mounted: function mounted() {// $(".has-treeview").click(function(){
    //     $(".has-treeview").not(this).each(function(){
    //         $(this).removeClass("menu-open");
    //         $(this).children("a").first().children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-left");
    //         $(this).children(".nav-treeview").slideUp();
    //     });
    // });
  },
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar
    };
  },
  methods: {
    subIsActive: function subIsActive(params) {
      var _this = this;

      var input = params.split(',');
      var paths = Array.isArray(input) ? input : [input];
      return paths.some(function (path) {
        return _this.$route.path.indexOf(path) === 0; // current path starts with this path string
      });
    },
    checkOpenMenu: function checkOpenMenu(menu) {
      if (menu == 1) {
        setTimeout(function () {
          if ($(".menu-layout").hasClass("menu-open")) {
            $('.menu-layout').removeClass('menu-open');
            $('.menu-layout').children(".nav-treeview").slideUp();
          } else {
            $('.menu-layout').addClass('menu-open');
            $('.menu-layout').children(".nav-treeview").slideDown();
          }
        }, 100);
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=template&id=b9f3f16e&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=template&id=b9f3f16e& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "aside",
      { staticClass: "main-sidebar sidebar-dark-primary elevation-4" },
      [
        _c(
          "router-link",
          { staticClass: "brand-link", attrs: { to: "/mosque" } },
          [
            _c("img", {
              staticClass: "brand-image img-circle elevation-3",
              attrs: { src: _vm.globalVar.publicPath + "images/logo.png" }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "brand-text font-weight-light" }, [
              _vm._v("\n        " + _vm._s(_vm.globalVar.appName) + "\n      ")
            ])
          ]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "sidebar" }, [
          _vm.globalVar.session
            ? _c("div", { staticClass: "user-panel mt-3 pb-3 mb-3 d-flex" }, [
                _c("div", { staticClass: "image" }, [
                  _c("img", {
                    staticClass: "img-circle elevation-2",
                    attrs: {
                      src: _vm.globalVar.publicPath + "images/default.png",
                      alt: "User Image"
                    }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "info sidebar-info",
                    staticStyle: { "white-space": "normal" }
                  },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "d-block",
                        attrs: { href: "javascript:;" }
                      },
                      [
                        _c("b", [
                          _vm._v(" " + _vm._s(_vm.globalVar.session.name) + " ")
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "d-block",
                        staticStyle: { "font-size": "12px" },
                        attrs: { href: "javascript:;" }
                      },
                      [
                        _vm._v(
                          "\n              " +
                            _vm._s(_vm.globalVar.session.mosque.name) +
                            "\n          "
                        )
                      ]
                    )
                  ]
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("nav", { staticClass: "mt-2" }, [
            _c(
              "ul",
              {
                staticClass: "nav nav-pills nav-sidebar flex-column",
                attrs: {
                  "data-widget": "treeview",
                  role: "menu",
                  "data-accordion": "false"
                }
              },
              [
                _c(
                  "li",
                  { staticClass: "nav-item d-none" },
                  [
                    _c(
                      "router-link",
                      { staticClass: "nav-link", attrs: { to: "/dashboard" } },
                      [
                        _c("i", { staticClass: "fas fa-home" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "  \n                      Dashboard\n                  "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      { staticClass: "nav-link", attrs: { to: "/mosque" } },
                      [
                        _c("i", { staticClass: "fas fa-mosque" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "  \n                      Informasi Masjid\n                  "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      { staticClass: "nav-link", attrs: { to: "/takmir" } },
                      [
                        _c("i", { staticClass: "fas fa-users" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "  \n                      Akun Takmir\n                  "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      { staticClass: "nav-link", attrs: { to: "/device" } },
                      [
                        _c("i", { staticClass: "fas fa-tv" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "  \n                      Media TV\n                  "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "nav-item has-treeview menu-layout",
                    class: {
                      "menu-open": _vm.subIsActive(
                        "/content/information,/content/clock"
                      )
                    },
                    on: {
                      click: function($event) {
                        return _vm.checkOpenMenu(1)
                      }
                    }
                  },
                  [
                    _c(
                      "a",
                      {
                        class: "nav-link menuid-content",
                        attrs: { href: "javascript:;" }
                      },
                      [
                        _c("i", { staticClass: "fas fa-columns" }),
                        _vm._v(" "),
                        _vm._m(0)
                      ]
                    ),
                    _vm._v(" "),
                    _c("ul", { staticClass: "nav nav-treeview" }, [
                      _c(
                        "li",
                        { staticClass: "nav-item" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "nav-link",
                              attrs: { to: "/content/clock" }
                            },
                            [
                              _c("small", [
                                _c("i", { staticClass: "fas fa-circle-notch" }),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(
                                    "  \n                                Hanya Jam\n                            "
                                  )
                                ])
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "router-link",
                            {
                              staticClass: "nav-link",
                              attrs: { to: "/content/informative" }
                            },
                            [
                              _c("small", [
                                _c("i", { staticClass: "fas fa-circle-notch" }),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(
                                    "  \n                                Informatif\n                            "
                                  )
                                ])
                              ])
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item d-none" },
                  [
                    _c(
                      "router-link",
                      { staticClass: "nav-link", attrs: { to: "/preacher" } },
                      [
                        _c("i", { staticClass: "fas fa-user-friends" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "  \n                      Khotip dan Bilal\n                  "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item d-none" },
                  [
                    _c(
                      "router-link",
                      { staticClass: "nav-link", attrs: { to: "/donature" } },
                      [
                        _c("i", { staticClass: "fas fa-user-tie" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "  \n                      Donatur\n                  "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "nav-item has-treeview d-none",
                    class: {
                      "menu-open": _vm.subIsActive(
                        "/setting/general,/setting/advanced,/setting/location,/setting/praying-time"
                      )
                    }
                  },
                  [
                    _c(
                      "a",
                      {
                        class: "nav-link menuid-setting",
                        attrs: { href: "javascript:;" }
                      },
                      [
                        _c("i", { staticClass: "fas fa-cogs" }),
                        _vm._v(" "),
                        _vm._m(1)
                      ]
                    ),
                    _vm._v(" "),
                    _c("ul", { staticClass: "nav nav-treeview" }, [
                      _c(
                        "li",
                        { staticClass: "nav-item" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "nav-link",
                              attrs: { to: "/setting/general" }
                            },
                            [
                              _c("small", [
                                _c("i", { staticClass: "fas fa-circle-notch" }),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(
                                    "  \n                                Umum\n                            "
                                  )
                                ])
                              ])
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "nav-item has-treeview d-none",
                    class: {
                      "menu-open": _vm.subIsActive(
                        "/text/running,/text/info,/text/arabic"
                      )
                    }
                  },
                  [
                    _c(
                      "a",
                      {
                        class: "nav-link menuid-text",
                        attrs: { href: "javascript:;" }
                      },
                      [
                        _c("i", { staticClass: "fas fa-paragraph" }),
                        _vm._v(" "),
                        _vm._m(2)
                      ]
                    ),
                    _vm._v(" "),
                    _c("ul", { staticClass: "nav nav-treeview" }, [
                      _c(
                        "li",
                        { staticClass: "nav-item" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "nav-link",
                              attrs: { to: "/text/running" }
                            },
                            [
                              _c("small", [
                                _c("i", { staticClass: "fas fa-circle-notch" }),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(
                                    "  \n                          Teks Berjalan\n                      "
                                  )
                                ])
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "nav-item" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "nav-link",
                              attrs: { to: "/text/info" }
                            },
                            [
                              _c("small", [
                                _c("i", { staticClass: "fas fa-circle-notch" }),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(
                                    "  \n                          Pesan Singkat\n                      "
                                  )
                                ])
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "nav-item" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "nav-link",
                              attrs: { to: "/text/arabic" }
                            },
                            [
                              _c("small", [
                                _c("i", { staticClass: "fas fa-circle-notch" }),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(
                                    "  \n                          Teks Arab dan Terjemahan\n                      "
                                  )
                                ])
                              ])
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "nav-item has-treeview d-none",
                    class: {
                      "menu-open": _vm.subIsActive(
                        "/image/background,/image/photo"
                      )
                    }
                  },
                  [
                    _c(
                      "a",
                      {
                        class: "nav-link menuid-text",
                        attrs: { href: "javascript:;" }
                      },
                      [
                        _c("i", { staticClass: "fas fa-image" }),
                        _vm._v(" "),
                        _vm._m(3)
                      ]
                    ),
                    _vm._v(" "),
                    _c("ul", { staticClass: "nav nav-treeview" }, [
                      _c(
                        "li",
                        { staticClass: "nav-item" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "nav-link",
                              attrs: { to: "/image/background" }
                            },
                            [
                              _c("small", [
                                _c("i", { staticClass: "fas fa-circle-notch" }),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(
                                    "  \n                          Slide Latar\n                      "
                                  )
                                ])
                              ])
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link",
                      attrs: { href: "javascript:;" },
                      on: { click: _vm.doLogout }
                    },
                    [
                      _c("i", { staticClass: "fas fa-power-off" }),
                      _vm._v(" "),
                      _c("p", [
                        _vm._v(
                          "  \n                      Sign Out\n                  "
                        )
                      ])
                    ]
                  )
                ])
              ]
            )
          ])
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v("  \n                Konten Layout\n                "),
      _c("i", { staticClass: "right fas fa-angle-left" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v("  \n                Pengaturan\n                "),
      _c("i", { staticClass: "right fas fa-angle-left" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v("  \n                Teks\n                "),
      _c("i", { staticClass: "right fas fa-angle-left" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v("  \n                Gambar\n                "),
      _c("i", { staticClass: "right fas fa-angle-left" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/reusable/Sidebar.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/backoffice/reusable/Sidebar.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Sidebar_vue_vue_type_template_id_b9f3f16e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=template&id=b9f3f16e& */ "./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=template&id=b9f3f16e&");
/* harmony import */ var _Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Sidebar_vue_vue_type_template_id_b9f3f16e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Sidebar_vue_vue_type_template_id_b9f3f16e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/reusable/Sidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=template&id=b9f3f16e&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=template&id=b9f3f16e& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_b9f3f16e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=template&id=b9f3f16e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/reusable/Sidebar.vue?vue&type=template&id=b9f3f16e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_b9f3f16e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_b9f3f16e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);