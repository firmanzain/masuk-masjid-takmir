(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/text-running"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/TextRunning.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/TextRunning.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var date = new Date();
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    this.loadData();
    Fire.$on('AfterLoaded', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    var sortOrders = {};
    var columns = [{
      width: '5%',
      label: 'No.',
      sortable: false
    }, {
      label: 'Teks',
      name: 'text',
      sortable: true
    }, {
      label: 'Lokasi',
      name: 'location',
      sortable: true
    }, {
      label: 'Mulai',
      name: 'start_at',
      sortable: true
    }, {
      label: 'Selesai',
      name: 'end_at',
      sortable: true
    }, {
      label: 'Status',
      name: 'status',
      sortable: true
    }, {
      width: '15%',
      label: 'Action',
      sortable: false
    }];
    columns.forEach(function (column) {
      sortOrders[column.name] = -1;
    });
    return {
      globalVar: this.$attrs.globalVar,
      renderingCountLocation: 0,
      selecteditemsLocation: [{
        id: 1,
        name: 'Hanya Jam'
      }, {
        id: 2,
        name: 'Informatif'
      }],
      dataTables: [],
      columns: columns,
      sortKey: 'name',
      sortOrders: sortOrders,
      perPage: ['10', '25', '50', '100'],
      tableData: {
        draw: 0,
        length: 10,
        search: '',
        column: 1,
        dir: 'asc',
        acp: 'masjid',
        layout: 1
      },
      pagination: {
        lastPage: '',
        currentPage: '',
        total: '',
        lastPageUrl: '',
        nextPageUrl: '',
        prevPageUrl: '',
        from: '',
        to: ''
      },
      form: new Form({
        id: '',
        text: '',
        start_at: new Date(new Date(date.getFullYear(), date.getMonth(), date.getDate())),
        end_at: new Date(new Date(date.getFullYear(), date.getMonth(), date.getDate())),
        location: '',
        always_show: false
      })
    };
  },
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  methods: {
    changeLocation: function changeLocation() {
      var self = this;
      var locationId = 0;

      if ($("#location").select2('data')[0]) {
        locationId = $("#location").select2('data')[0].id;
      }

      self.form.location = parseInt(locationId);
    },
    configPagination: function configPagination(data) {
      this.pagination.lastPage = data.last_page;
      this.pagination.currentPage = data.current_page;
      this.pagination.total = data.total;
      this.pagination.lastPageUrl = data.last_page_url;
      this.pagination.nextPageUrl = data.next_page_url;
      this.pagination.prevPageUrl = data.prev_page_url;
      this.pagination.from = data.from;
      this.pagination.to = data.to;
    },
    sortBy: function sortBy(key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
      this.tableData.column = this.getIndex(this.columns, 'name', key);
      this.tableData.dir = this.sortOrders[key] === 1 ? 'asc' : 'desc';
      this.loadData();
    },
    getIndex: function getIndex(array, key, value) {
      return array.findIndex(function (i) {
        return i[key] == value;
      });
    },
    loadData: function loadData() {
      var self = this;
      self.deleteDefaultHeaders();

      if (self.$route.params && self.$route.params.layout) {
        if (self.$route.params.layout == 'clock') {
          self.tableData.layout = 1;
        } else if (self.$route.params.layout == 'informative') {
          self.tableData.layout = 2;
        }
      }

      self.tableData.draw++;
      axios.get("".concat(self.globalVar.apiUrl, "text/running"), {
        params: self.tableData,
        headers: self.globalVar.headers
      }).then(function (response) {
        var data = response.data;

        if (self.tableData.draw == data.draw) {
          if (self.globalVar.fromLayout != 0) {
            self.dataTables = [];

            for (var i = 0; i < data.data.data.length; i++) {
              if (data.data.data[i].location == self.globalVar.fromLayout) {
                self.dataTables.push(data.data.data[i]);
              }
            }
          } else {
            self.dataTables = data.data.data;
          }

          self.configPagination(data.data);
        }
      })["catch"](function (errors) {});
    },
    openModal: function openModal() {
      var _this2 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      $("#location").select2("destroy");
      this.form.clear();
      this.form.reset();
      this.form.start_at = new Date(new Date(date.getFullYear(), date.getMonth(), date.getDate()));
      this.form.end_at = new Date(new Date(date.getFullYear(), date.getMonth(), date.getDate()));
      $("#modalForm").modal("show");

      if (data) {
        this.form.fill(data);

        if (this.form.location == 1) {
          document.getElementById("location").selectedIndex = 0;
        } else {
          document.getElementById("location").selectedIndex = 1;
        }

        if (!data.start_at) {
          this.form.start_at = this.formatDateDefault(new Date(new Date(date.getFullYear(), date.getMonth(), date.getDate())));
        }

        if (!data.end_at) {
          this.form.end_at = this.formatDateDefault(new Date(new Date(date.getFullYear(), date.getMonth(), date.getDate())));
        }
      }

      $("#location").select2({
        width: '100%'
      });
      setTimeout(function () {
        _this2.$refs.text.focus();
      }, 400);
    },
    submitData: function submitData() {
      var self = this;
      self.$Progress.start();
      self.showProgressBar();
      this.deleteDefaultHeaders();
      var method = '';
      self.form.start_at = self.formatDateDefault(self.form.start_at);
      self.form.end_at = self.formatDateDefault(self.form.end_at);
      this.form.location = $("#location").select2('data')[0].id;

      if (self.form.id) {
        method = axios.put("".concat(self.globalVar.apiUrl, "text/running/").concat(self.form.id), self.form, {
          headers: self.globalVar.headers,
          onUploadProgress: function onUploadProgress(progressEvent) {
            var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100));
            $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
          }
        });
      } else {
        method = axios.post("".concat(self.globalVar.apiUrl, "text/running"), self.form, {
          headers: self.globalVar.headers,
          onUploadProgress: function onUploadProgress(progressEvent) {
            var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100));
            $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
          }
        });
      }

      method.then(function (response) {
        Toast.fire({
          type: 'success',
          title: 'Data updated successfully'
        });
        self.$Progress.finish();
        Fire.$emit('AfterLoaded');
        self.showPassword = false;
        $("#modalForm").modal("hide");
      })["catch"](function (error) {
        var message = 'Something went wrong.';

        if (error.response) {
          message = error.response.data.message;
          Swal.fire('Alert!', message, 'warning');
        }

        self.$Progress.fail();
      });
    },
    activeData: function activeData(data) {
      var _this3 = this;

      var self = this;
      var status = 0;
      var text = 'Record will be Deactivated';

      if (data.status == 0) {
        status = 1;
        text = 'Record will be Activated';
      }

      Swal.fire({
        title: 'Are you sure?',
        text: text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          _this3.deleteDefaultHeaders();

          axios.put("".concat(self.globalVar.apiUrl, "text/running/status/").concat(data.id), {
            status: status
          }, {
            headers: self.globalVar.headers
          }).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterLoaded');
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    },
    deleteData: function deleteData(data) {
      var _this4 = this;

      var self = this;
      Swal.fire({
        title: 'Are you sure?',
        text: 'Record will be deleted.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then(function (result) {
        if (result.value) {
          _this4.deleteDefaultHeaders();

          axios["delete"]("".concat(self.globalVar.apiUrl, "text/running/").concat(data.id), {
            headers: self.globalVar.headers
          }).then(function (response) {
            Toast.fire({
              type: 'success',
              title: 'Data saved successfully'
            });
            Fire.$emit('AfterLoaded');
          })["catch"](function (error) {
            Swal.fire('Alert!', 'Something went wrong.', 'warning');
          });
        }
      });
    },
    statusLabelWithDate: function statusLabelWithDate(data) {
      if (data.status == 0) {
        return '<span class="badge badge-danger">Non Aktif</span>';
      } else {
        if (data.always_show == 1) {
          return '<span class="badge badge-success">Aktif</span>';
        } else {
          if (moment__WEBPACK_IMPORTED_MODULE_0___default()(data.start_at).isAfter(moment__WEBPACK_IMPORTED_MODULE_0___default()())) {
            return '<span class="badge badge-warning">Pending</span>';
          } else if (moment__WEBPACK_IMPORTED_MODULE_0___default()(data.end_at).isBefore(moment__WEBPACK_IMPORTED_MODULE_0___default()())) {
            return '<span class="badge badge-danger">Expired</span>';
          } else {
            return '<span class="badge badge-success">Aktif</span>';
          }
        }
      }
    }
  },
  watch: {
    'form.start_at': function formStart_at(to, from) {
      var start_at = moment__WEBPACK_IMPORTED_MODULE_0___default()(this.form.start_at);
      var end_at = moment__WEBPACK_IMPORTED_MODULE_0___default()(this.form.end_at);

      if (moment__WEBPACK_IMPORTED_MODULE_0___default()(start_at).isAfter(end_at)) {
        this.form.start_at = this.formatDateDefault(end_at);
      }
    },
    'form.end_at': function formEnd_at(to, from) {
      var start_at = moment__WEBPACK_IMPORTED_MODULE_0___default()(this.form.start_at);
      var end_at = moment__WEBPACK_IMPORTED_MODULE_0___default()(this.form.end_at);

      if (moment__WEBPACK_IMPORTED_MODULE_0___default()(end_at).isBefore(start_at)) {
        this.form.end_at = this.formatDateDefault(start_at);
      }
    }
  },
  activated: function activated() {
    var self = this;
    Fire.$emit('AfterLoaded');
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/TextRunning.vue?vue&type=template&id=08dff1de&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/TextRunning.vue?vue&type=template&id=08dff1de& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Teks Berjalan")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-header" }, [
                _c("h3", { staticClass: "card-title" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: { click: _vm.goback }
                    },
                    [
                      _c("i", { staticClass: "fas fa-arrow-left" }),
                      _vm._v(
                        "   Kembali ke layout\n                            "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "card-tools" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: { click: _vm.openModal }
                    },
                    [
                      _c("i", { staticClass: "fas fa-plus-circle" }),
                      _vm._v("   Tambah Data\n                            ")
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("br")
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "card-body dataTables_wrapper scroll" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "dataTables_length",
                      attrs: { id: "tableData_length" }
                    },
                    [
                      _c("label", [
                        _vm._v("Show\n                                "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.tableData.length,
                                expression: "tableData.length"
                              }
                            ],
                            on: {
                              change: [
                                function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.tableData,
                                    "length",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                },
                                function($event) {
                                  return _vm.loadData()
                                }
                              ]
                            }
                          },
                          _vm._l(_vm.perPage, function(records, index) {
                            return _c(
                              "option",
                              { key: index, domProps: { value: records } },
                              [_vm._v(_vm._s(records))]
                            )
                          }),
                          0
                        ),
                        _vm._v("\n                                entries")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "dataTables_filter",
                      attrs: { id: "tableData_filter" }
                    },
                    [
                      _c("label", [
                        _vm._v("Search:\n                                "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.tableData.search,
                              expression: "tableData.search"
                            }
                          ],
                          attrs: {
                            type: "text",
                            placeholder: "",
                            "aria-controls": "tableData"
                          },
                          domProps: { value: _vm.tableData.search },
                          on: {
                            input: [
                              function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.tableData,
                                  "search",
                                  $event.target.value
                                )
                              },
                              function($event) {
                                return _vm.loadData()
                              }
                            ]
                          }
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "datatable",
                    {
                      attrs: {
                        columns: _vm.columns,
                        sortKey: _vm.sortKey,
                        sortOrders: _vm.sortOrders
                      },
                      on: { sort: _vm.sortBy }
                    },
                    [
                      _vm.dataTables.length != 0
                        ? _c(
                            "tbody",
                            _vm._l(_vm.dataTables, function(dataTable, index) {
                              return _c("tr", { key: index }, [
                                _c("td", [_vm._v(_vm._s(index + 1))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(dataTable.text))]),
                                _vm._v(" "),
                                dataTable.location == 1
                                  ? _c("td", [
                                      _vm._v(
                                        "\n                                        Hanya Jam\n                                    "
                                      )
                                    ])
                                  : _c("td", [
                                      _vm._v(
                                        "\n                                        Informatif\n                                    "
                                      )
                                    ]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(
                                    _vm._s(
                                      _vm._f("formatDate")(dataTable.start_at)
                                    )
                                  )
                                ]),
                                _vm._v(" "),
                                dataTable.always_show == 0
                                  ? _c("td", [
                                      _vm._v(
                                        "\n                                      " +
                                          _vm._s(
                                            _vm._f("formatDate")(
                                              dataTable.end_at
                                            )
                                          ) +
                                          "\n                                    "
                                      )
                                    ])
                                  : _c("td", [
                                      _vm._v(
                                        "\n                                      -\n                                    "
                                      )
                                    ]),
                                _vm._v(" "),
                                _c("td", {
                                  domProps: {
                                    innerHTML: _vm._s(
                                      _vm.statusLabelWithDate(dataTable)
                                    )
                                  }
                                }),
                                _vm._v(" "),
                                _c("td", { staticClass: "text-center" }, [
                                  dataTable.status == 1
                                    ? _c(
                                        "button",
                                        {
                                          staticClass: "btn btn-danger btn-sm",
                                          attrs: {
                                            type: "button",
                                            title: "Non Aktifkan"
                                          },
                                          on: {
                                            click: function($event) {
                                              return _vm.activeData(dataTable)
                                            }
                                          }
                                        },
                                        [_c("i", { staticClass: "fas fa-ban" })]
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  dataTable.status == 0
                                    ? _c(
                                        "button",
                                        {
                                          staticClass: "btn btn-success btn-sm",
                                          attrs: {
                                            type: "button",
                                            title: "Aktifkan"
                                          },
                                          on: {
                                            click: function($event) {
                                              return _vm.activeData(dataTable)
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fas fa-check-circle"
                                          })
                                        ]
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-primary btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Ubah Data"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openModal(dataTable)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-edit" })]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-danger btn-sm",
                                      attrs: {
                                        type: "button",
                                        title: "Hapus Data"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteData(dataTable)
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-trash-alt"
                                      })
                                    ]
                                  )
                                ])
                              ])
                            }),
                            0
                          )
                        : _c("tbody", [
                            _c("tr", [
                              _c(
                                "td",
                                {
                                  staticClass: "text-center",
                                  attrs: { colspan: "7" }
                                },
                                [
                                  _vm._v(
                                    "\n                                      Tidak ada data yang cocok ditemukan\n                                    "
                                  )
                                ]
                              )
                            ])
                          ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("pagination", {
                    attrs: { pagination: _vm.pagination },
                    on: {
                      prev: function($event) {
                        return _vm.loadData(_vm.pagination.prevPageUrl)
                      },
                      next: function($event) {
                        return _vm.loadData(_vm.pagination.nextPageUrl)
                      }
                    }
                  })
                ],
                1
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade bd-example-modal-lg",
        attrs: {
          id: "modalForm",
          role: "dialog",
          "aria-labelledby": "modalFormLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog modal-lg", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _c("div", { staticClass: "modal-header" }, [
                !_vm.form.id
                  ? _c(
                      "h5",
                      {
                        staticClass: "modal-title",
                        attrs: { id: "modalFormLabel" }
                      },
                      [
                        _vm._v(
                          "\n            Tambah Data Teks Berjalan\n          "
                        )
                      ]
                    )
                  : _c(
                      "h5",
                      {
                        staticClass: "modal-title",
                        attrs: { id: "modalFormLabel" }
                      },
                      [
                        _vm._v(
                          "\n            Ubah Data Teks Berjalan\n          "
                        )
                      ]
                    ),
                _vm._v(" "),
                _vm._m(2)
              ]),
              _vm._v(" "),
              _c(
                "form",
                {
                  attrs: { id: "formAdd" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.submitData($event)
                    }
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "modal-body" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.id,
                            expression: "form.id"
                          }
                        ],
                        staticClass: "d-none",
                        class: { "is-invalid": _vm.form.errors.has("id") },
                        attrs: { type: "text", name: "id", id: "id" },
                        domProps: { value: _vm.form.id },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "id", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("has-error", {
                        attrs: { form: _vm.form, field: "id" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "text" }
                          },
                          [
                            _vm._v(
                              "\n                      Teks\n                  "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.form.text,
                                  expression: "form.text"
                                }
                              ],
                              ref: "text",
                              staticClass: "form-control",
                              class: {
                                "is-invalid": _vm.form.errors.has("text")
                              },
                              attrs: {
                                name: "text",
                                id: "text",
                                rows: "4",
                                placeholder: "Teks",
                                required: ""
                              },
                              domProps: { value: _vm.form.text },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.form,
                                    "text",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "text" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "location" }
                          },
                          [
                            _vm._v(
                              "\n                    Lokasi\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            key: _vm.renderingCountLocation,
                            staticClass: "col-sm-8"
                          },
                          [
                            _c("select2", {
                              ref: "location",
                              attrs: {
                                url: "",
                                name: "location",
                                selecteditems: _vm.selecteditemsLocation,
                                identifier: "location",
                                placeholder: "Lokasi",
                                required: ""
                              },
                              on: { changed: _vm.changeLocation }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "location" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "start_at" }
                          },
                          [
                            _vm._v(
                              "\n                      Tanggal Mulai\n                  "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c("datepicker", {
                              ref: "start_at",
                              class: {
                                "is-invalid": _vm.form.errors.has("start_at")
                              },
                              attrs: {
                                "input-class": "form-control",
                                name: "start_at",
                                id: "start_at",
                                placeholder: "Tanggal Mulai",
                                autocomplete: "off",
                                format: _vm.formatDateDefault,
                                required: ""
                              },
                              model: {
                                value: _vm.form.start_at,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "start_at", $$v)
                                },
                                expression: "form.start_at"
                              }
                            }),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "start_at" }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "form-group row",
                          class: _vm.form.always_show ? "d-none" : ""
                        },
                        [
                          _c(
                            "label",
                            {
                              staticClass: "col-sm-4 col-form-label",
                              attrs: { for: "end_at" }
                            },
                            [
                              _vm._v(
                                "\n                      Tanggal Selesai\n                  "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-sm-8" },
                            [
                              _c("datepicker", {
                                ref: "end_at",
                                class: {
                                  "is-invalid": _vm.form.errors.has("end_at")
                                },
                                attrs: {
                                  "input-class": "form-control",
                                  name: "end_at",
                                  id: "end_at",
                                  placeholder: "Tanggal Selesai",
                                  autocomplete: "off",
                                  format: _vm.formatDateDefault,
                                  required: ""
                                },
                                model: {
                                  value: _vm.form.end_at,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "end_at", $$v)
                                  },
                                  expression: "form.end_at"
                                }
                              }),
                              _vm._v(" "),
                              _c("has-error", {
                                attrs: { form: _vm.form, field: "end_at" }
                              })
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group row" }, [
                        _c(
                          "label",
                          {
                            staticClass: "col-sm-4 col-form-label",
                            attrs: { for: "always_show" }
                          },
                          [
                            _vm._v(
                              "\n                  Selalu Ditampilkan\n                "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8" },
                          [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "custom-control custom-switch pointer"
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.form.always_show,
                                      expression: "form.always_show"
                                    }
                                  ],
                                  ref: "always_show",
                                  staticClass: "custom-control-input",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "always_show"
                                    )
                                  },
                                  attrs: {
                                    type: "checkbox",
                                    id: "always_show",
                                    name: "always_show"
                                  },
                                  domProps: {
                                    checked: Array.isArray(_vm.form.always_show)
                                      ? _vm._i(_vm.form.always_show, null) > -1
                                      : _vm.form.always_show
                                  },
                                  on: {
                                    change: function($event) {
                                      var $$a = _vm.form.always_show,
                                        $$el = $event.target,
                                        $$c = $$el.checked ? true : false
                                      if (Array.isArray($$a)) {
                                        var $$v = null,
                                          $$i = _vm._i($$a, $$v)
                                        if ($$el.checked) {
                                          $$i < 0 &&
                                            _vm.$set(
                                              _vm.form,
                                              "always_show",
                                              $$a.concat([$$v])
                                            )
                                        } else {
                                          $$i > -1 &&
                                            _vm.$set(
                                              _vm.form,
                                              "always_show",
                                              $$a
                                                .slice(0, $$i)
                                                .concat($$a.slice($$i + 1))
                                            )
                                        }
                                      } else {
                                        _vm.$set(_vm.form, "always_show", $$c)
                                      }
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                !_vm.form.always_show
                                  ? _c(
                                      "label",
                                      {
                                        staticClass: "custom-control-label",
                                        attrs: { for: "always_show" }
                                      },
                                      [_vm._v("Tidak Aktif")]
                                    )
                                  : _c(
                                      "label",
                                      {
                                        staticClass: "custom-control-label",
                                        attrs: { for: "always_show" }
                                      },
                                      [_vm._v("Aktif")]
                                    )
                              ]
                            ),
                            _vm._v(" "),
                            _c("has-error", {
                              attrs: { form: _vm.form, field: "always_show" }
                            })
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm._m(3)
                ]
              )
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Teks Berjalan")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Teks")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer text-right" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n                  Batal\n              ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-success", attrs: { type: "submit" } },
        [_vm._v("\n                  Simpan\n              ")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/TextRunning.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/backoffice/TextRunning.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TextRunning_vue_vue_type_template_id_08dff1de___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TextRunning.vue?vue&type=template&id=08dff1de& */ "./resources/js/components/backoffice/TextRunning.vue?vue&type=template&id=08dff1de&");
/* harmony import */ var _TextRunning_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TextRunning.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/TextRunning.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TextRunning_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TextRunning_vue_vue_type_template_id_08dff1de___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TextRunning_vue_vue_type_template_id_08dff1de___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/TextRunning.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/TextRunning.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/backoffice/TextRunning.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TextRunning_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TextRunning.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/TextRunning.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TextRunning_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/TextRunning.vue?vue&type=template&id=08dff1de&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/backoffice/TextRunning.vue?vue&type=template&id=08dff1de& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TextRunning_vue_vue_type_template_id_08dff1de___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TextRunning.vue?vue&type=template&id=08dff1de& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/TextRunning.vue?vue&type=template&id=08dff1de&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TextRunning_vue_vue_type_template_id_08dff1de___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TextRunning_vue_vue_type_template_id_08dff1de___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);