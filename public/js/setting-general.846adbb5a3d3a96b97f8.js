(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/setting-general"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/SettingGeneral.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/SettingGeneral.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuejs_timepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuejs-timepicker */ "./node_modules/vuejs-timepicker/dist/vue2-timepicker.min.js");
/* harmony import */ var vuejs_timepicker__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuejs_timepicker__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    this.loadData();
    Fire.$on('AfterLoaded', function () {
      _this.loadData();
    });
  },
  mounted: function mounted() {},
  data: function data() {
    return {
      globalVar: this.$attrs.globalVar,
      form: new Form({
        id_subuh: '',
        start_at_subuh: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        azan_duration_subuh: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        iqamat_duration_subuh: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        solat_duration_subuh: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        id_zuhur: '',
        start_at_zuhur: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        azan_duration_zuhur: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        iqamat_duration_zuhur: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        solat_duration_zuhur: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        id_asar: '',
        start_at_asar: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        azan_duration_asar: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        iqamat_duration_asar: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        solat_duration_asar: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        id_magrib: '',
        start_at_magrib: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        azan_duration_magrib: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        iqamat_duration_magrib: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        solat_duration_magrib: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        id_isya: '',
        start_at_isya: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        azan_duration_isya: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        iqamat_duration_isya: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        solat_duration_isya: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        id_jumat: '',
        start_at_jumat: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        azan_duration_jumat: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        iqamat_duration_jumat: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        solat_duration_jumat: {
          HH: "00",
          mm: "00",
          ss: "00"
        },
        set_jumat_start: false
      })
    };
  },
  components: {
    VueTimepicker: vuejs_timepicker__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  methods: {
    loadData: function loadData() {
      var self = this;
      self.deleteDefaultHeaders();
      axios.get("".concat(self.globalVar.apiUrl, "setting/general"), {
        params: {
          acp: 'masjid'
        },
        headers: self.globalVar.headers
      }).then(function (response) {
        var split = [];

        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].name == 'subuh') {
            self.form.id_subuh = response.data[i].id;
            split = response.data[i].start_at.split(":");
            self.form.start_at_subuh = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].azan_duration.split(":");
            self.form.azan_duration_subuh = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].iqamat_duration.split(":");
            self.form.iqamat_duration_subuh = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].solat_duration.split(":");
            self.form.solat_duration_subuh = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
          } else if (response.data[i].name == 'zuhur') {
            self.form.id_zuhur = response.data[i].id;
            split = response.data[i].start_at.split(":");
            self.form.start_at_zuhur = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].azan_duration.split(":");
            self.form.azan_duration_zuhur = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].iqamat_duration.split(":");
            self.form.iqamat_duration_zuhur = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].solat_duration.split(":");
            self.form.solat_duration_zuhur = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
          } else if (response.data[i].name == 'asar') {
            self.form.id_asar = response.data[i].id;
            split = response.data[i].start_at.split(":");
            self.form.start_at_asar = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].azan_duration.split(":");
            self.form.azan_duration_asar = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].iqamat_duration.split(":");
            self.form.iqamat_duration_asar = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].solat_duration.split(":");
            self.form.solat_duration_asar = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
          } else if (response.data[i].name == 'magrib') {
            self.form.id_magrib = response.data[i].id;
            split = response.data[i].start_at.split(":");
            self.form.start_at_magrib = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].azan_duration.split(":");
            self.form.azan_duration_magrib = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].iqamat_duration.split(":");
            self.form.iqamat_duration_magrib = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].solat_duration.split(":");
            self.form.solat_duration_magrib = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
          } else if (response.data[i].name == 'isya') {
            self.form.id_isya = response.data[i].id;
            split = response.data[i].start_at.split(":");
            self.form.start_at_isya = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].azan_duration.split(":");
            self.form.azan_duration_isya = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].iqamat_duration.split(":");
            self.form.iqamat_duration_isya = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].solat_duration.split(":");
            self.form.solat_duration_isya = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
          } else if (response.data[i].name == 'jumat') {
            self.form.id_jumat = response.data[i].id;
            split = response.data[i].start_at.split(":");
            self.form.start_at_jumat = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            self.form.set_jumat_start = true;

            if (self.form.start_at_jumat.HH == "00" && self.form.start_at_jumat.mm == "00" && self.form.start_at_jumat.ss == "00") {
              self.form.set_jumat_start = false;
            }

            split = response.data[i].azan_duration.split(":");
            self.form.azan_duration_jumat = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].iqamat_duration.split(":");
            self.form.iqamat_duration_jumat = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
            split = response.data[i].solat_duration.split(":");
            self.form.solat_duration_jumat = {
              HH: split[0],
              mm: split[1],
              ss: split[2]
            };
          }
        }
      })["catch"](function (error) {
        if (error.response) {}
      });
    },
    submitData: function submitData() {
      var self = this;
      console.log("===form", self.form);
      self.$Progress.start();
      this.deleteDefaultHeaders();
      self.$Progress.start();
      self.showProgressBar(); // SUBUH

      axios.put("".concat(self.globalVar.apiUrl, "setting/general/").concat(self.form.id_subuh), {
        // name: 'subuh',
        start_at: self.form.start_at_subuh.HH + ':' + self.form.start_at_subuh.mm + ':' + self.form.start_at_subuh.ss,
        azan_duration: self.form.azan_duration_subuh.HH + ':' + self.form.azan_duration_subuh.mm + ':' + self.form.azan_duration_subuh.ss,
        iqamat_duration: self.form.iqamat_duration_subuh.HH + ':' + self.form.iqamat_duration_subuh.mm + ':' + self.form.iqamat_duration_subuh.ss,
        solat_duration: self.form.solat_duration_subuh.HH + ':' + self.form.solat_duration_subuh.mm + ':' + self.form.solat_duration_subuh.ss
      }, {
        headers: self.globalVar.headers,
        onUploadProgress: function onUploadProgress(progressEvent) {
          var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100)) / 6;
          $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
        }
      }).then(function (response) {
        self.$Progress.finish();
      })["catch"](function (error) {
        self.$Progress.fail();
      });
      self.$Progress.start(); // ZUHUR

      axios.put("".concat(self.globalVar.apiUrl, "setting/general/").concat(self.form.id_zuhur), {
        // name: 'zuhur',
        start_at: self.form.start_at_zuhur.HH + ':' + self.form.start_at_zuhur.mm + ':' + self.form.start_at_zuhur.ss,
        azan_duration: self.form.azan_duration_zuhur.HH + ':' + self.form.azan_duration_zuhur.mm + ':' + self.form.azan_duration_zuhur.ss,
        iqamat_duration: self.form.iqamat_duration_zuhur.HH + ':' + self.form.iqamat_duration_zuhur.mm + ':' + self.form.iqamat_duration_zuhur.ss,
        solat_duration: self.form.solat_duration_zuhur.HH + ':' + self.form.solat_duration_zuhur.mm + ':' + self.form.solat_duration_zuhur.ss
      }, {
        headers: self.globalVar.headers,
        onUploadProgress: function onUploadProgress(progressEvent) {
          var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100)) / 6 + 15;
          $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
        }
      }).then(function (response) {
        self.$Progress.finish();
      })["catch"](function (error) {
        self.$Progress.fail();
      });
      self.$Progress.start(); // ASAR

      axios.put("".concat(self.globalVar.apiUrl, "setting/general/").concat(self.form.id_asar), {
        // name: 'asar',
        start_at: self.form.start_at_asar.HH + ':' + self.form.start_at_asar.mm + ':' + self.form.start_at_asar.ss,
        azan_duration: self.form.azan_duration_asar.HH + ':' + self.form.azan_duration_asar.mm + ':' + self.form.azan_duration_asar.ss,
        iqamat_duration: self.form.iqamat_duration_asar.HH + ':' + self.form.iqamat_duration_asar.mm + ':' + self.form.iqamat_duration_asar.ss,
        solat_duration: self.form.solat_duration_asar.HH + ':' + self.form.solat_duration_asar.mm + ':' + self.form.solat_duration_asar.ss
      }, {
        headers: self.globalVar.headers,
        onUploadProgress: function onUploadProgress(progressEvent) {
          var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100)) / 6 + 30;
          $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
        }
      }).then(function (response) {
        self.$Progress.finish();
      })["catch"](function (error) {
        self.$Progress.fail();
      });
      self.$Progress.start(); // MAGRIB

      axios.put("".concat(self.globalVar.apiUrl, "setting/general/").concat(self.form.id_magrib), {
        // name: 'magrib',
        start_at: self.form.start_at_magrib.HH + ':' + self.form.start_at_magrib.mm + ':' + self.form.start_at_magrib.ss,
        azan_duration: self.form.azan_duration_magrib.HH + ':' + self.form.azan_duration_magrib.mm + ':' + self.form.azan_duration_magrib.ss,
        iqamat_duration: self.form.iqamat_duration_magrib.HH + ':' + self.form.iqamat_duration_magrib.mm + ':' + self.form.iqamat_duration_magrib.ss,
        solat_duration: self.form.solat_duration_magrib.HH + ':' + self.form.solat_duration_magrib.mm + ':' + self.form.solat_duration_magrib.ss
      }, {
        headers: self.globalVar.headers,
        onUploadProgress: function onUploadProgress(progressEvent) {
          var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100)) / 6 + 45;
          $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
        }
      }).then(function (response) {
        self.$Progress.finish();
      })["catch"](function (error) {
        self.$Progress.fail();
      });
      self.$Progress.start(); // ISYA

      axios.put("".concat(self.globalVar.apiUrl, "setting/general/").concat(self.form.id_isya), {
        // name: 'isya',
        start_at: self.form.start_at_isya.HH + ':' + self.form.start_at_isya.mm + ':' + self.form.start_at_isya.ss,
        azan_duration: self.form.azan_duration_isya.HH + ':' + self.form.azan_duration_isya.mm + ':' + self.form.azan_duration_isya.ss,
        iqamat_duration: self.form.iqamat_duration_isya.HH + ':' + self.form.iqamat_duration_isya.mm + ':' + self.form.iqamat_duration_isya.ss,
        solat_duration: self.form.solat_duration_isya.HH + ':' + self.form.solat_duration_isya.mm + ':' + self.form.solat_duration_isya.ss
      }, {
        headers: self.globalVar.headers,
        onUploadProgress: function onUploadProgress(progressEvent) {
          var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100)) / 6 + 60;
          $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
        }
      }).then(function (response) {
        self.$Progress.finish();
      })["catch"](function (error) {
        self.$Progress.fail();
      });
      self.$Progress.start(); // JUMAT

      axios.put("".concat(self.globalVar.apiUrl, "setting/general/").concat(self.form.id_jumat), {
        // name: 'jumat',
        start_at: self.form.start_at_jumat.HH + ':' + self.form.start_at_jumat.mm + ':' + self.form.start_at_jumat.ss,
        azan_duration: self.form.azan_duration_jumat.HH + ':' + self.form.azan_duration_jumat.mm + ':' + self.form.azan_duration_jumat.ss,
        iqamat_duration: self.form.iqamat_duration_jumat.HH + ':' + self.form.iqamat_duration_jumat.mm + ':' + self.form.iqamat_duration_jumat.ss,
        solat_duration: self.form.solat_duration_jumat.HH + ':' + self.form.solat_duration_jumat.mm + ':' + self.form.solat_duration_jumat.ss
      }, {
        headers: self.globalVar.headers,
        onUploadProgress: function onUploadProgress(progressEvent) {
          var progressValue = parseInt(Math.round(progressEvent.loaded / progressEvent.total * 100)) / 6 + 85;
          $("#progress").attr("aria-valuenow", progressValue).css("width", progressValue + "%");
        }
      }).then(function (response) {
        Toast.fire({
          type: 'success',
          title: 'Data updated successfully'
        });
        self.$Progress.finish();
        Fire.$emit('AfterLoaded');
        self.$Progress.finish();
      })["catch"](function (error) {
        self.$Progress.fail();
      });
    }
  },
  watch: {
    'form.set_jumat_start': function formSet_jumat_start(to, from) {
      if (to == false) {
        this.form.start_at_jumat.HH = "00";
        this.form.start_at_jumat.mm = "00";
        this.form.start_at_jumat.ss = "00";
      }
    }
  },
  activated: function activated() {
    var self = this;
    self.loadData();
    Fire.$emit('AfterLoaded');
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/SettingGeneral.vue?vue&type=template&id=7b45f809&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backoffice/SettingGeneral.vue?vue&type=template&id=7b45f809& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm._v(_vm._s(_vm.globalVar.appName))
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Umum")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "content" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-header" }, [
                _c("h3", { staticClass: "card-title" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: { click: _vm.goback }
                    },
                    [
                      _c("i", { staticClass: "fas fa-arrow-left" }),
                      _vm._v("   Kembali ke layout\n                          ")
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "card-tools" }),
                _vm._v(" "),
                _c("br")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "form",
                  {
                    attrs: { id: "formAdd" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submitData($event)
                      }
                    }
                  },
                  [
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "div",
                        { staticClass: "col-sm-6 border-right" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.id_subuh,
                                expression: "form.id_subuh"
                              }
                            ],
                            staticClass: "d-none",
                            class: {
                              "is-invalid": _vm.form.errors.has("id_subuh")
                            },
                            attrs: {
                              type: "text",
                              name: "id_subuh",
                              id: "id_subuh"
                            },
                            domProps: { value: _vm.form.id_subuh },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "id_subuh",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "id_subuh" }
                          }),
                          _vm._v(" "),
                          _vm._m(2),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "azan_duration_subuh" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Azan\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "azan_duration_subuh",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_subuh"
                                    )
                                  },
                                  attrs: {
                                    name: "azan_duration_subuh",
                                    id: "azan_duration_subuh",
                                    placeholder: "Durasi Azan",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.azan_duration_subuh,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "azan_duration_subuh",
                                        $$v
                                      )
                                    },
                                    expression: "form.azan_duration_subuh"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "azan_duration_subuh"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "iqamat_duration_subuh" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Iqamat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "iqamat_duration_subuh",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_subuh"
                                    )
                                  },
                                  attrs: {
                                    name: "iqamat_duration_subuh",
                                    id: "iqamat_duration_subuh",
                                    placeholder: "Durasi Iqamat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.iqamat_duration_subuh,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "iqamat_duration_subuh",
                                        $$v
                                      )
                                    },
                                    expression: "form.iqamat_duration_subuh"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "iqamat_duration_subuh"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "solat_duration_subuh" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Solat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "solat_duration_subuh",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_subuh"
                                    )
                                  },
                                  attrs: {
                                    name: "solat_duration_subuh",
                                    id: "solat_duration_subuh",
                                    placeholder: "Durasi Solat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.solat_duration_subuh,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "solat_duration_subuh",
                                        $$v
                                      )
                                    },
                                    expression: "form.solat_duration_subuh"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "solat_duration_subuh"
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-6" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.id_zuhur,
                                expression: "form.id_zuhur"
                              }
                            ],
                            staticClass: "d-none",
                            class: {
                              "is-invalid": _vm.form.errors.has("id_zuhur")
                            },
                            attrs: {
                              type: "text",
                              name: "id_zuhur",
                              id: "id_zuhur"
                            },
                            domProps: { value: _vm.form.id_zuhur },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "id_zuhur",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "id_zuhur" }
                          }),
                          _vm._v(" "),
                          _vm._m(3),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "azan_duration_zuhur" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Azan\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "azan_duration_zuhur",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_zuhur"
                                    )
                                  },
                                  attrs: {
                                    name: "azan_duration_zuhur",
                                    id: "azan_duration_zuhur",
                                    placeholder: "Durasi Azan",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.azan_duration_zuhur,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "azan_duration_zuhur",
                                        $$v
                                      )
                                    },
                                    expression: "form.azan_duration_zuhur"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "azan_duration_zuhur"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "iqamat_duration_zuhur" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Iqamat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "iqamat_duration_zuhur",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_zuhur"
                                    )
                                  },
                                  attrs: {
                                    name: "iqamat_duration_zuhur",
                                    id: "iqamat_duration_zuhur",
                                    placeholder: "Durasi Iqamat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.iqamat_duration_zuhur,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "iqamat_duration_zuhur",
                                        $$v
                                      )
                                    },
                                    expression: "form.iqamat_duration_zuhur"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "iqamat_duration_zuhur"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "solat_duration_zuhur" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Solat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "solat_duration_zuhur",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_zuhur"
                                    )
                                  },
                                  attrs: {
                                    name: "solat_duration_zuhur",
                                    id: "solat_duration_zuhur",
                                    placeholder: "Durasi Solat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.solat_duration_zuhur,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "solat_duration_zuhur",
                                        $$v
                                      )
                                    },
                                    expression: "form.solat_duration_zuhur"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "solat_duration_zuhur"
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "div",
                        { staticClass: "col-sm-6 border-right" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.id_asar,
                                expression: "form.id_asar"
                              }
                            ],
                            staticClass: "d-none",
                            class: {
                              "is-invalid": _vm.form.errors.has("id_asar")
                            },
                            attrs: {
                              type: "text",
                              name: "id_asar",
                              id: "id_asar"
                            },
                            domProps: { value: _vm.form.id_asar },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "id_asar",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "id_asar" }
                          }),
                          _vm._v(" "),
                          _vm._m(4),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "azan_duration_asar" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Azan\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "azan_duration_asar",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_asar"
                                    )
                                  },
                                  attrs: {
                                    name: "azan_duration_asar",
                                    id: "azan_duration_asar",
                                    placeholder: "Durasi Azan",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.azan_duration_asar,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "azan_duration_asar",
                                        $$v
                                      )
                                    },
                                    expression: "form.azan_duration_asar"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "azan_duration_asar"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "iqamat_duration_asar" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Iqamat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "iqamat_duration_asar",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_asar"
                                    )
                                  },
                                  attrs: {
                                    name: "iqamat_duration_asar",
                                    id: "iqamat_duration_asar",
                                    placeholder: "Durasi Iqamat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.iqamat_duration_asar,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "iqamat_duration_asar",
                                        $$v
                                      )
                                    },
                                    expression: "form.iqamat_duration_asar"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "iqamat_duration_asar"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "solat_duration_asar" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Solat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "solat_duration_asar",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_asar"
                                    )
                                  },
                                  attrs: {
                                    name: "solat_duration_asar",
                                    id: "solat_duration_asar",
                                    placeholder: "Durasi Solat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.solat_duration_asar,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "solat_duration_asar",
                                        $$v
                                      )
                                    },
                                    expression: "form.solat_duration_asar"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "solat_duration_asar"
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-6" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.id_magrib,
                                expression: "form.id_magrib"
                              }
                            ],
                            staticClass: "d-none",
                            class: {
                              "is-invalid": _vm.form.errors.has("id_magrib")
                            },
                            attrs: {
                              type: "text",
                              name: "id_magrib",
                              id: "id_magrib"
                            },
                            domProps: { value: _vm.form.id_magrib },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "id_magrib",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "id_magrib" }
                          }),
                          _vm._v(" "),
                          _vm._m(5),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "azan_duration_magrib" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Azan\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "azan_duration_magrib",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_magrib"
                                    )
                                  },
                                  attrs: {
                                    name: "azan_duration_magrib",
                                    id: "azan_duration_magrib",
                                    placeholder: "Durasi Azan",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.azan_duration_magrib,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "azan_duration_magrib",
                                        $$v
                                      )
                                    },
                                    expression: "form.azan_duration_magrib"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "azan_duration_magrib"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "iqamat_duration_magrib" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Iqamat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "iqamat_duration_magrib",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_magrib"
                                    )
                                  },
                                  attrs: {
                                    name: "iqamat_duration_magrib",
                                    id: "iqamat_duration_magrib",
                                    placeholder: "Durasi Iqamat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.iqamat_duration_magrib,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "iqamat_duration_magrib",
                                        $$v
                                      )
                                    },
                                    expression: "form.iqamat_duration_magrib"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "iqamat_duration_magrib"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "solat_duration_magrib" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Solat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "solat_duration_magrib",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_magrib"
                                    )
                                  },
                                  attrs: {
                                    name: "solat_duration_magrib",
                                    id: "solat_duration_magrib",
                                    placeholder: "Durasi Solat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.solat_duration_magrib,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "solat_duration_magrib",
                                        $$v
                                      )
                                    },
                                    expression: "form.solat_duration_magrib"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "solat_duration_magrib"
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "div",
                        { staticClass: "col-sm-6 border-right" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.id_isya,
                                expression: "form.id_isya"
                              }
                            ],
                            staticClass: "d-none",
                            class: {
                              "is-invalid": _vm.form.errors.has("id_isya")
                            },
                            attrs: {
                              type: "text",
                              name: "id_isya",
                              id: "id_isya"
                            },
                            domProps: { value: _vm.form.id_isya },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "id_isya",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "id_isya" }
                          }),
                          _vm._v(" "),
                          _vm._m(6),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "azan_duration_isya" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Azan\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "azan_duration_isya",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_isya"
                                    )
                                  },
                                  attrs: {
                                    name: "azan_duration_isya",
                                    id: "azan_duration_isya",
                                    placeholder: "Durasi Azan",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.azan_duration_isya,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "azan_duration_isya",
                                        $$v
                                      )
                                    },
                                    expression: "form.azan_duration_isya"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "azan_duration_isya"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "iqamat_duration_isya" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Iqamat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "iqamat_duration_isya",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_isya"
                                    )
                                  },
                                  attrs: {
                                    name: "iqamat_duration_isya",
                                    id: "iqamat_duration_isya",
                                    placeholder: "Durasi Iqamat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.iqamat_duration_isya,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "iqamat_duration_isya",
                                        $$v
                                      )
                                    },
                                    expression: "form.iqamat_duration_isya"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "iqamat_duration_isya"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "solat_duration_isya" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Solat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "solat_duration_isya",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_isya"
                                    )
                                  },
                                  attrs: {
                                    name: "solat_duration_isya",
                                    id: "solat_duration_isya",
                                    placeholder: "Durasi Solat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.solat_duration_isya,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "solat_duration_isya",
                                        $$v
                                      )
                                    },
                                    expression: "form.solat_duration_isya"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "solat_duration_isya"
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-6" },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.id_jumat,
                                expression: "form.id_jumat"
                              }
                            ],
                            staticClass: "d-none",
                            class: {
                              "is-invalid": _vm.form.errors.has("id_jumat")
                            },
                            attrs: {
                              type: "text",
                              name: "id_jumat",
                              id: "id_jumat"
                            },
                            domProps: { value: _vm.form.id_jumat },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "id_jumat",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: { form: _vm.form, field: "id_jumat" }
                          }),
                          _vm._v(" "),
                          _vm._m(7),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "set_jumat_start" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Atur Jam Mulai\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "custom-control custom-switch pointer"
                                  },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.set_jumat_start,
                                          expression: "form.set_jumat_start"
                                        }
                                      ],
                                      ref: "set_jumat_start",
                                      staticClass: "custom-control-input",
                                      class: {
                                        "is-invalid": _vm.form.errors.has(
                                          "set_jumat_start"
                                        )
                                      },
                                      attrs: {
                                        type: "checkbox",
                                        id: "set_jumat_start",
                                        name: "set_jumat_start"
                                      },
                                      domProps: {
                                        checked: Array.isArray(
                                          _vm.form.set_jumat_start
                                        )
                                          ? _vm._i(
                                              _vm.form.set_jumat_start,
                                              null
                                            ) > -1
                                          : _vm.form.set_jumat_start
                                      },
                                      on: {
                                        change: function($event) {
                                          var $$a = _vm.form.set_jumat_start,
                                            $$el = $event.target,
                                            $$c = $$el.checked ? true : false
                                          if (Array.isArray($$a)) {
                                            var $$v = null,
                                              $$i = _vm._i($$a, $$v)
                                            if ($$el.checked) {
                                              $$i < 0 &&
                                                _vm.$set(
                                                  _vm.form,
                                                  "set_jumat_start",
                                                  $$a.concat([$$v])
                                                )
                                            } else {
                                              $$i > -1 &&
                                                _vm.$set(
                                                  _vm.form,
                                                  "set_jumat_start",
                                                  $$a
                                                    .slice(0, $$i)
                                                    .concat($$a.slice($$i + 1))
                                                )
                                            }
                                          } else {
                                            _vm.$set(
                                              _vm.form,
                                              "set_jumat_start",
                                              $$c
                                            )
                                          }
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    !_vm.form.set_jumat_start
                                      ? _c(
                                          "label",
                                          {
                                            staticClass: "custom-control-label",
                                            attrs: { for: "set_jumat_start" }
                                          },
                                          [_vm._v("Tidak Aktif")]
                                        )
                                      : _c(
                                          "label",
                                          {
                                            staticClass: "custom-control-label",
                                            attrs: { for: "set_jumat_start" }
                                          },
                                          [_vm._v("Aktif")]
                                        )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "set_jumat_start"
                                  }
                                })
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _vm.form.set_jumat_start
                            ? _c("div", { staticClass: "form-group row" }, [
                                _c(
                                  "label",
                                  {
                                    staticClass: "col-sm-6 col-form-label",
                                    attrs: { for: "start_at_jumat" }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                            Jam Mulai\n                                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-sm-6" },
                                  [
                                    _c("vue-timepicker", {
                                      ref: "start_at_jumat",
                                      class: {
                                        "is-invalid": _vm.form.errors.has(
                                          "start_at_jumat"
                                        )
                                      },
                                      attrs: {
                                        name: "start_at_jumat",
                                        id: "start_at_jumat",
                                        placeholder: "Jam Mulai",
                                        format: "HH:mm:ss",
                                        "hide-clear-button": "",
                                        required: ""
                                      },
                                      model: {
                                        value: _vm.form.start_at_jumat,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.form,
                                            "start_at_jumat",
                                            $$v
                                          )
                                        },
                                        expression: "form.start_at_jumat"
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("has-error", {
                                      attrs: {
                                        form: _vm.form,
                                        field: "start_at_jumat"
                                      }
                                    })
                                  ],
                                  1
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-sm-6 col-form-label",
                                attrs: { for: "solat_duration_jumat" }
                              },
                              [
                                _vm._v(
                                  "\n                                            Durasi Solat\n                                        "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-sm-6" },
                              [
                                _c("vue-timepicker", {
                                  ref: "solat_duration_jumat",
                                  class: {
                                    "is-invalid": _vm.form.errors.has(
                                      "azan_duration_jumat"
                                    )
                                  },
                                  attrs: {
                                    name: "solat_duration_jumat",
                                    id: "solat_duration_jumat",
                                    placeholder: "Durasi Solat",
                                    format: "HH:mm:ss",
                                    "hide-clear-button": "",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.form.solat_duration_jumat,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.form,
                                        "solat_duration_jumat",
                                        $$v
                                      )
                                    },
                                    expression: "form.solat_duration_jumat"
                                  }
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: {
                                    form: _vm.form,
                                    field: "solat_duration_jumat"
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _vm._m(8)
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Umum")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "breadcrumb-item" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [_vm._v("Pengaturan")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-center text-uppercase" }, [
        _c("h4", [_vm._v("Subuh")]),
        _c("br")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-center text-uppercase" }, [
        _c("h4", [_vm._v("Zuhur")]),
        _c("br")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-center text-uppercase" }, [
        _c("h4", [_vm._v("Asar")]),
        _c("br")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-center text-uppercase" }, [
        _c("h4", [_vm._v("Magrib")]),
        _c("br")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-center text-uppercase" }, [
        _c("h4", [_vm._v("Isya")]),
        _c("br")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-center text-uppercase" }, [
        _c("h4", [_vm._v("Jumat")]),
        _c("br")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row" }, [
      _c("div", { staticClass: "col-sm-12 text-right" }, [
        _c(
          "button",
          { staticClass: "btn btn-success", attrs: { type: "submit" } },
          [
            _vm._v(
              "\n                                        Simpan\n                                    "
            )
          ]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backoffice/SettingGeneral.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/backoffice/SettingGeneral.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SettingGeneral_vue_vue_type_template_id_7b45f809___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SettingGeneral.vue?vue&type=template&id=7b45f809& */ "./resources/js/components/backoffice/SettingGeneral.vue?vue&type=template&id=7b45f809&");
/* harmony import */ var _SettingGeneral_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SettingGeneral.vue?vue&type=script&lang=js& */ "./resources/js/components/backoffice/SettingGeneral.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SettingGeneral_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SettingGeneral_vue_vue_type_template_id_7b45f809___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SettingGeneral_vue_vue_type_template_id_7b45f809___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backoffice/SettingGeneral.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backoffice/SettingGeneral.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/backoffice/SettingGeneral.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingGeneral_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SettingGeneral.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/SettingGeneral.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingGeneral_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backoffice/SettingGeneral.vue?vue&type=template&id=7b45f809&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/backoffice/SettingGeneral.vue?vue&type=template&id=7b45f809& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingGeneral_vue_vue_type_template_id_7b45f809___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SettingGeneral.vue?vue&type=template&id=7b45f809& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backoffice/SettingGeneral.vue?vue&type=template&id=7b45f809&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingGeneral_vue_vue_type_template_id_7b45f809___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingGeneral_vue_vue_type_template_id_7b45f809___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);