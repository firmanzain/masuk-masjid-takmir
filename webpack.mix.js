const mix = require('laravel-mix');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .webpackConfig({
      output: {
        chunkFilename: '[name].[contenthash].js',
        path: path.resolve(process.cwd(), 'public'),
      },
      plugins: [
        new CleanWebpackPlugin({
        cleanOnceBeforeBuildPatterns: [
          'css/*',
          'js/*',
          '!.htaccess',
          '!favicon.ico',
          '!index.php',
          '!robots.txt',
          '!web.config',
          '!images'
        ]
        }),
      ],
    })
    .options({
    });
